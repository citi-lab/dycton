#include <stdlib.h>

#include "hal.h"
#include "address_map.h"
#include "dycton_dummy_memset_app.h"    
#include "libdycton.h"

int main() {
    char *string0 = malloc(200);
    char *string1 = malloc(500);
    char *string2 = malloc(1000);
    char *string3 = malloc(5000);
    char *string4 = malloc(10000);

    for (int i = 0; i < 5000; i++) {
        string0[i % 10] = 'v';
    }
    for (int i = 0; i < 2500; i++) {
        string1[i % 10] = 'v';
    }
    for (int i = 0; i < 1000; i++) {
        string2[i % 10] = 'v';
    }
    for (int i = 0; i < 800; i++) {
        string3[i % 10] = 'v';
    }
    for (int i = 0; i < 200; i++) {
        string4[i % 10] = 'v';
    }
    free(string0);
    free(string1);
    free(string2);
    free(string3);
    free(string4);
    exit(0);
    return 0;
}