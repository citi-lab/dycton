#ifndef DYCTON_DUMMY_MEMSET_APP_H
#define DYCTON_DUMMY_MEMSET_APP_H


#define DYCTON_RUN

#define DATASET_COUNT 		(8)

const char * dataset_array[DATASET_COUNT] = {"useless", 
											"useless",
											"useless",
											"useless",
											"useless",
											"useless",
											"useless",
											"useless"};
											
uint32_t dataset_footprint[DATASET_COUNT] = {	17200, 
												17200,
												17200,
												17200,
												17200,
												17200,
												17200,
												17200};


uint32_t dataset_in_size[DATASET_COUNT] = {	0,
											0,
											0,
											0,
											0,
											0,
											0,
											0};

#endif //DYCTON_DIJKSTRA_APP_H