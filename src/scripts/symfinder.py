#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
import argparse
import sys

def symfinder(filepath=None, address=None):
  # command line arguments processing
  if filepath is None or address is None or not path.exists(filepath):
    print("error: invalid input.")
    return 1, None

  with open(filepath, "r") as infile:
    for line in infile:
      try:
        cur_sym_addr, _, _, section, size, symbol_name = line.split()
        cur_sym_addr, size = int(cur_sym_addr, 16), int(size, 16)
        if isinstance(address, str):
          address = int(address, 16)
        if section == ".text" and address >= cur_sym_addr and address < (cur_sym_addr + size):
          output = f"{symbol_name} {address - cur_sym_addr}"
          print(output)
          return 0, output 
      except:
        pass
  print("error: not found")
  return 1, None




if __name__ == "__main__":
  # command line arguments processing
  epilog = ("standard output: the function name and the hexadecimal offset from the function start or \"ERROR ERROR\"")
  parser = argparse.ArgumentParser(epilog=epilog)
  parser.add_argument("filepath", metavar="FILEPATH", help="the path to the symbol table in textformat (objdump -t target.out > symtable.txt)", action="store", required=True)
  parser.add_argument("address", metavar="ADDRESS", help="the code address to link with a function name /!\\ INTERPRETED AS HEXADECIMAL", action="store", required=True, type=lambda x: int(x, 16))
  args = parser.parse_args()
  return_val, _ = symfinder(**vars(args))
  sys.exit(return_val)

