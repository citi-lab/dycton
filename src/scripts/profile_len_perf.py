#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from os import mkdir, path
import sys
import csv
import argparse
import matplotlib.pyplot as plt
import numpy as np


# multi_dataset representation
dataset_look_and_feel = {
  "_d0_":{"color":"blue" , "linestyle": '-', "marker": "o", "fillstyle": "none"},
  "_d1_":{"color":"blue" , "linestyle": '--', "marker": "o", "fillstyle": "full"},
  "_d2_":{"color":"red" , "linestyle": '-', "marker": "s", "fillstyle": "none"},
  "_d3_":{"color":"red" , "linestyle": '--', "marker": "s", "fillstyle": "full"},
  "_d4_":{"color":"green" , "linestyle": '-', "marker": "^", "fillstyle": "none"},
  "_d5_":{"color":"green" , "linestyle": '--', "marker": "^", "fillstyle": "full"},
  "_d6_":{"color":"black" , "linestyle": '-', "marker": "v", "fillstyle": "none"},
  "_d7_":{"color":"black" , "linestyle": '--', "marker": "v", "fillstyle": "full"}
}

def profile_len_perf(arch, app, ref_csv, dataset, graphics=False):
  # start by printing the date
  date_start = str(datetime.now())

  print("Dycton profile length evaluation script")
  print("========================================")
  print("author: T. Delizy (delizy.tristan@gmail.com)")
  print(date_start)
  print("")

  if app not in ["h263", "dijkstra", "json_parser", "jpg2000", "ecdsa", "dummy_memset"]:
    print("application unrecognized, exiting...")
    return 2
  if arch not in range(1,100):
    print("architecture unrecognized, exiting...")
    return 2
  if ref_csv is None or not path.exists(ref_csv):
  	print("please provide input csv file with -i, exiting...")
  	return 2

  print("input csv:", ref_csv)
  print("target application:", app)
  print("target architecture:", arch)
  print("target dataset:", "all" if dataset == None else f"d{dataset}")
  print("- - - - - - - - - - - - - - - - - - - - - - - - - ")

  print("==================================")
  print("parsing input values")
  print("==================================")
  with open(ref_csv, newline="") as csvfile:
    data_reader = csv.DictReader(csvfile)
    data = list(filter(lambda row: dataset is None or f"_d{dataset}_" in row["Experience"], data_reader))
    for row in data:
      if int(row["Return Code"]) != 0:
        print("error: one experimentation have invalid return code.")
        return 1
    print("dataset filtered:")
    uniques_dset_str = [f"_d{i}_" for i in ([dataset] if not dataset is None else list(set([row["Experience"].split("_d")[1].split("_")[0] for row in data])))]

  # selecting target dataset
  # highest_heap_obj = allocs[np.where((allocs[:,0]+allocs[:,1]) == max(allocs[:,0]+allocs[:,1]))[0][0]]

  # horrible multi dataset refactor
  ref_ilp_raw = {}
  ref_ilp = {}
  ref_density_raw = {}
  ref_density = {}
  ref_upper_bound = {}
  ref_baseline = {}
  time_ref = {}

  list_len_for = {}
  speedup_for = {}
  fallback_for = {}
  list_len_switch = {}
  speedup_switch = {}
  fallback_switch = {}
  for_count = {}
  max_len = {}
  switch_count = {}

  baseline = {}
  ilp = {}
  upper_bound = {}
  density = {}

  for dataset_str in uniques_dset_str:

    print("working on dataset", dataset_str)
    # print "data[0]", data[0]['experience']
    # print "test ?", "yes" if dataset_str in data[0]['experience'] else "no"
    # print "np.where(dataset_str in data['experience'])", np.where(np.char.find(data['experience'],dataset_str)>=0)
    # print "filtered data:\n", data[np.where(np.char.find(data['experience'],dataset_str)>=0)[0]]


    ref_ilp_raw[dataset_str] = []
    ref_ilp[dataset_str]  = -1
    ref_density_raw[dataset_str]  = []
    ref_density[dataset_str]  = -1
    ref_upper_bound[dataset_str]  = -1
    ref_baseline[dataset_str]  = -1
    time_ref[dataset_str]  = -1
    arch_data = []

    for d in list(filter(lambda row: dataset_str in row["Experience"], data)):
      if f"arch{arch}" in d["Experience"] and app in d["Experience"]:
        arch_data.append(d)

      if "arch0" in d["Experience"] and app in d["Experience"]:
        time_ref[dataset_str] = int(d["Final Cycles"])
        print("time_ref = ", time_ref[dataset_str])
      if f"arch{arch}" in d["Experience"] and app in d["Experience"]:
        if "baseline" in d["Experience"]:
          ref_baseline[dataset_str] = int(d["Final Cycles"])
          print(f"arch{arch} baseline:", ref_baseline[dataset_str])
        if "ilp" in d["Experience"]:
          if "ilp_upper_bound" in d["Experience"]:
            ref_upper_bound[dataset_str] = int(d["Final Cycles"])
          else:
            ref_ilp_raw[dataset_str].append(int(d["Final Cycles"]))
        if "density" in d["Experience"]:
          ref_density_raw[dataset_str].append(int(d["Final Cycles"]))

    if len(ref_ilp_raw[dataset_str]) > 0:
      ref_ilp[dataset_str] = min(ref_ilp_raw[dataset_str])

    if len(ref_density_raw[dataset_str]) > 0:
      ref_density[dataset_str] = min(ref_density_raw[dataset_str])


    if ref_baseline[dataset_str] < 0 or time_ref[dataset_str] < 0:
      print("Error in reference retrieving (we need baseline and reference time), exiting...")
      print("dataset :", dataset_str)
      print("ref_ilp =", ref_ilp[dataset_str])
      print("ref_ilp_raw =", ref_ilp_raw[dataset_str])
      print("ref_density =", ref_density[dataset_str])
      print("ref_density_raw =", ref_density_raw[dataset_str])
      print("ref_baseline =", ref_baseline[dataset_str])
      print("ref_upper_bound =", ref_upper_bound[dataset_str])
      print("time_ref =", time_ref[dataset_str])
      return 2

    list_len_for[dataset_str] = []
    speedup_for[dataset_str] = []
    fallback_for[dataset_str] = []
    list_len_switch[dataset_str] = []
    speedup_switch[dataset_str] = []
    fallback_switch[dataset_str] = []
    for_count[dataset_str] = 0
    max_len[dataset_str] = 0
    switch_count[dataset_str] = 0
    for d in arch_data:
      if "profile" in d["Experience"]:
        prof_len = int(d["Experience"].split("profile_")[1].split("_run")[0])
        list_len_switch[dataset_str].append(prof_len)
        if prof_len > max_len[dataset_str]:
          max_len[dataset_str] = prof_len
        speedup_switch[dataset_str].append((1-float(d["Final Cycles"])/time_ref[dataset_str])*100)
        fallback_switch[dataset_str].append(float(d["Fallback"]))
        switch_count[dataset_str] += 1

    baseline[dataset_str] = (1-float(ref_baseline[dataset_str])/time_ref[dataset_str])*100
    if ref_ilp[dataset_str] > 0:
      ilp[dataset_str] = (1-float(ref_ilp[dataset_str])/time_ref[dataset_str])*100
    if ref_density[dataset_str] > 0:
      density[dataset_str] = (1-float(ref_density[dataset_str])/time_ref[dataset_str])*100
    if ref_upper_bound[dataset_str] >0:
      upper_bound[dataset_str] = (1-float(ref_upper_bound[dataset_str])/time_ref[dataset_str])*100

    if len(list_len_for[dataset_str])> 0:
      list_len_for[dataset_str] = np.array(list_len_for[dataset_str])
      speedup_for[dataset_str] = np.array(speedup_for[dataset_str])
      fallback_for[dataset_str] = np.array(fallback_for[dataset_str])
      indices_for = np.argsort(list_len_for[dataset_str])
      list_len_for[dataset_str] = list_len_for[dataset_str][indices_for]
      speedup_for[dataset_str] = speedup_for[dataset_str][indices_for]
      fallback_for[dataset_str] = fallback_for[dataset_str][indices_for]

    if len(list_len_switch[dataset_str])> 0:
      list_len_switch[dataset_str] = np.array(list_len_switch[dataset_str])
      speedup_switch[dataset_str] = np.array(speedup_switch[dataset_str])
      fallback_switch[dataset_str] = np.array(fallback_switch[dataset_str])
      indices_switch = np.argsort(list_len_switch[dataset_str])
      list_len_switch[dataset_str] = list_len_switch[dataset_str][indices_switch]
      speedup_switch[dataset_str] = speedup_switch[dataset_str][indices_switch]
      fallback_switch[dataset_str] = fallback_switch[dataset_str][indices_switch]


  print("==================================")
  print("plotting")
  print("==================================")

  plt.close('all')
  f, ax1 = plt.subplots(figsize=(14, 10))



  # ax1.plot((1, max_len), (baseline,baseline), lw=1, color='blue')
  # ax1.text( max(for_count,switch_count)-0.5,baseline, "baseline speedup", horizontalalignment='right', color='blue')

  # if dataset != -1:
  #   ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

  for dataset_str in uniques_dset_str:
    if len(list_len_switch[dataset_str])> 0:
      ax1.plot(list_len_switch[dataset_str], speedup_switch[dataset_str], lw=1.5,
        color=dataset_look_and_feel[dataset_str]["color"],
        linestyle= dataset_look_and_feel[dataset_str]["linestyle"],
        marker= dataset_look_and_feel[dataset_str]["marker"],
        fillstyle=dataset_look_and_feel[dataset_str]["fillstyle"],
        label=f" {dataset_str} Profile Speedup")

    if not dataset is None:
      if ref_ilp[dataset_str] > 0:
        ax1.plot((1, max_len[dataset_str]), (ilp[dataset_str],ilp[dataset_str]), lw=1, color='red')
        ax1.text( 1.5,ilp[dataset_str], "ilp speedup", color='red', verticalalignment='top')

      if ref_density[dataset_str] > 0:
        ax1.plot((1, max_len[dataset_str]), (density[dataset_str],density[dataset_str]), lw=1, color='green')
        ax1.text( 1.5,density[dataset_str], "density speedup", color='green', verticalalignment='top')

      if ref_upper_bound[dataset_str] >0:
        ax1.plot((1, max_len[dataset_str]), (upper_bound[dataset_str],upper_bound[dataset_str]), lw=1, color='darkred')
        ax1.text( 1.5,upper_bound[dataset_str], "upper bound", color='darkred')

  ax1.set_xlim([1, max(max_len.values())])
  ax1.set_xlabel('Length of Allocation Site List')
  ax1.set_ylabel('Speedup against Reference Execution (%)')
  size = f.get_size_inches()*f.dpi
  ax1.set_title("Influence of profile length for "+app+" on architecture "+str(arch))
  ax1.legend(loc='lower left', bbox_to_anchor=(1, 0), ncol=1)
  f.subplots_adjust(top=0.9, right=0.8)


  if not path.exists("profiles_perf"):
    mkdir("profiles_perf")

  if dataset is None:
    plt.savefig(path.join("profiles_perf", app+"_arch"+str(arch)+"_all_datasets_profile_length_influence.pdf"), dpi=330)
  else:
    plt.savefig(path.join("profiles_perf", app+"_arch"+str(arch)+"_d"+str(dataset)+"_profile_length_influence.pdf"), dpi=330)


  if graphics:
    plt.show()

  print("done... exiting.")
  return 0

if __name__ == "__main__":
  # command line arguments processing
  epilog = ("example: ./profile_len_perf.py -a 5 -i input.csv -t h263")
  parser = argparse.ArgumentParser(epilog=epilog)
  parser.add_argument("-a", help="memory configuration, acceptable values in [1,99]", metavar="arch", dest="arch", action="store", type=int, required=True)
  parser.add_argument("-i", help="input csv file path", metavar="filepath", dest="ref_csv", action="store", required=True)
  parser.add_argument("-t", help="target application name.", metavar="target_application", dest="app", action="store", required=True)
  parser.add_argument("-d", help=" target dataset execution, if none, will print all datasets available", metavar="dataset", dest="dataset", action="store")
  parser.add_argument("--nographics", dest="graphics", help="toggle off plots rendering", action="store_false")
  args = parser.parse_args()
  return_val = profile_len_perf(**vars(args))
  sys.exit(return_val)


