#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from os import path, listdir
import argparse
import csv
import sys

def profile_len_explored(name):
  if "profile" not in name:
    return -3
  selstr = name.split("profile")[1]
  sel_nbr = [int(s) for s in selstr.split("_") if s.isdigit()]
  if len(sel_nbr) > 1:
    print("ambiguous profile exploration experiment run name :", name)
    return -2
  elif len(sel_nbr) == 0:
    return -1 # not a profile exploration
  else:
    return sel_nbr[0]


def result_synthesis(results_folder):
  # start by printing the date
  date_start = str(datetime.now())
  print("Result synthesis (CSV) construction from experiment logs")
  print("========================================================")
  print("author: T. Delizy (delizy.tristan@gmail.com)")
  print(date_start)

  prof_explo_res = {}

  if results_folder is None or not path.exists(results_folder):
    print("error: invalid raw results folder.")
    return 1

  # get the result file liste (its called "ls")
  total_error = 0
  result_extension = ".result"
  profile_extension = ".profile"
  errored_experience = []
  result_extension_len = len(result_extension)
  profile_extension_len = len(profile_extension)
  result_list = sorted([f for f in listdir(results_folder) if path.isfile(path.join(results_folder, f)) and f.endswith(result_extension)])
  profile_list = sorted([f for f in listdir(results_folder) if path.isfile(path.join(results_folder, f)) and f.endswith(profile_extension)])

  if len(result_list) == 0 and len(profile_list) == 0:
    print("This folder is empty of .result and .profile. Are you sure you provided the results_raw/ folder ?")
    return 1

  with open(path.join(results_folder, "../results/result_synthesis.csv"), 'w', newline='') as csvfile:
    synthesis = csv.writer(csvfile)
    synthesis.writerow(["Experience", "Return Code", "Final Cycles", "Allocator Cycles", "Fallback"])
    

    for result_file in result_list:

      name = result_file[:-result_extension_len]

      return_code = -1
      final_cycle = -1
      alloc_cycle = -1
      alloc_fallback = -1
      alloc_error = False
      with open(path.join(results_folder, result_file), "r") as resfile:

        for resline in resfile:
          if "[helper] stopping simulation by software trigger at time" in resline:
            return_code = int(resline.split(" = ")[1])
            # print "\treturn code =", return_code
          elif "final cycle count" in resline:
            final_cycle = int(resline.split(": ")[1][:-2])
            # print "\tfinal cycles =", final_cycle
          elif "total allocator cycle count" in resline:
            alloc_cycle = int(resline.split(": ")[1].split(" ")[0])
            # print "\talloc cycles", alloc_cycle
          elif "malloc fallback" in resline:
            alloc_fallback = float(resline.split("(")[1].split("%")[0])
          elif "outside of heap" in resline:
            alloc_error = True


      if return_code == -1 or final_cycle == -1 or alloc_cycle == -1 or alloc_fallback == -1:
        print(f"error: incomplete data for {name}.")
        errored_experience.append(result_file)
        total_error += 1
      elif return_code != 0:
        print(f"error: error in run {name}, exit code different from 0.")
        errored_experience.append(result_file)
        total_error += 1
      elif alloc_error == True:
        print(f"error: allocation outside of heap for {name}.")
        errored_experience.append(result_file)
        total_error += 1
      else:
        print(f"\t {name} ok.")

      csvline = [name, return_code, final_cycle, alloc_cycle, alloc_fallback]

      synthesis.writerow(csvline)

  if total_error == 0:
    print("success : experiment run without errors.")
  else:
    print(f"{total_error}" + " errored simulation has been detected in simulations : \n\t " + '\n\t '.join(errored_experience))
  return total_error

if __name__ == "__main__":
  # command line arguments processing
  epilog = ("constructs a CSV file containing information regarding experiment executions\n"
            "each line describe one execution of the simulator (name, exec time, return value, ...)\n"
            "for strategies implying profile exploration, best profile length is selected.")
  parser = argparse.ArgumentParser(epilog=epilog)
  parser.add_argument("results_folder", help="raw results folder path", action="store")
  args = parser.parse_args()
  return_val = result_synthesis(**vars(args))
  sys.exit(return_val)