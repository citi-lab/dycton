#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path, mkdir
from datetime import datetime
from shutil import move, copyfile
from time import sleep
import argparse
import sys
import subprocess
import re

sys.path.append('../../')

parentfolder = path.dirname(__file__)

# executables generation script
def generate_executable(arch_desc_only=False, exec_only=False, res_folder=None):
  # start by printing the date
  date = str(datetime.now())
  print("Executables and memory architecture description files generation for Dycton")
  print("====================================================================================================")
  print("author: T. Delizy (delizy.tristan@gmail.com)")
  print(date)

  if res_folder is None or not path.exists(res_folder):
    print("error: missing folder.")
    return 1

  if arch_desc_only and exec_only :
    print("error: '-a' and '-x' option are exclusives.")
    return 1

  # preparing subfolder results
  try:
    for folder in ["emb_softs", "simulators", "archi_desc"]:
      folder_path = path.join(res_folder, folder)
      if not path.exists(folder_path):
        mkdir(folder_path)
  except:
    print("error: cannot create experiment subfolders.")
    return 1


  try:
    import xp_config as xp
    print("config file imported.")
  except:
    print("config import failed.")
    return 1

  target_sw = xp.target_sw
  target_hw = xp.target_hw
  if any(x in xp.target_strats for x in ["profile"]):
    target_datasets = [0, 1, 2, 3, 4, 5, 6, 7]
  else:
    target_datasets = xp.target_datasets

  print("target_sw", target_sw)
  print("target_hw", target_hw)

  # calculate all percentage tested :
  percentages = set((int(x.split("_")[1]) for x in xp.target_strats if re.match(r"(^ilp_\d{1,2}$)|(^density_\d{1,2}$)", x)))

  # build execs :
  for sw in target_sw:
    soft_cmd = f"DY_SOFT={sw}"
    sim_name = f"{sw}_sim.x"
    soft_name = f"{sw}_soft.out"
    print("\n\nbuilding,", soft_cmd)
    p = subprocess.Popen(["make", "-B", soft_cmd], cwd=path.join(parentfolder, "../platform_tlm/iss/"), stdout=sys.stdout, stderr=sys.stdout)
    p.wait()
    if p.returncode:
      print("ERROR in simulator compilation, aborting")
      sys.exit(1)

    if arch_desc_only == True:
      for hw in target_hw:
        for d in target_datasets:

          # generate architecture description file associated
          p = subprocess.Popen(["./run.x", "-c", "-a", hw, "-d", str(d)], cwd=path.join(parentfolder, "../platform_tlm/iss/"), stdout=sys.stdout, stderr=sys.stdout)
          sleep(2)
          p.kill()

          # retrieve the architecture description file
          destination_file = path.join(res_folder, "archi_desc", f"{sw}_arch{hw}_d{d}_100p.desc")
          try:
            move(path.join(parentfolder, "../platform_tlm/logs/memory_architecture"), destination_file)
          except:
            print(f"error: unable to move memory architecture to {destination_file}.")
            return 1

          # build the x% architecture description file
          if hw not in ["0", "100"]:
            heap_desc = ""
            other_heap_desc = []
            with open(destination_file, "r") as infile:
              for line in infile:
                if line.startswith("HEAP_0:"):
                  heap_desc = line
                elif bool(line.strip()):
                  other_heap_desc.append(line)
            for percent in percentages:
              with open(path.join(res_folder, "archi_desc", f"{sw}_arch{hw}_d{d}_{percent}p.desc"), "w") as outfile:
                newline = heap_desc.split(":")
                newline[2] = str(int(round(int(newline[2]) * (percent/100))))
                newline = ":".join(newline)
                outfile.write(newline + "".join(other_heap_desc))

    elif exec_only == True:
      # retrieve the simulator exec
      destination_file = path.join(res_folder, "simulators", sim_name)
      try:
        move(path.join(parentfolder, "../platform_tlm/iss/run.x"), destination_file)
      except:
        print(f"error: unable to move run.x to {destination_file}.")
        return 1

      # retrieve the embedded software
      destination_file = path.join(res_folder, "emb_softs", soft_name)
      try:
        move(path.join(parentfolder, "../platform_tlm/software/a.out"), destination_file)
      except:
        print(f"error: unable to move a.out to {destination_file}.")
        return 1

      # retrieve the symbol table for profile computation
      destination_file = path.join(res_folder, "emb_softs", sw, "symbol_table.txt")
      try:
        mkdir(path.join(res_folder, "emb_softs", sw))
        copyfile(path.join(parentfolder, f"../platform_tlm/software/{sw}/cross/symbol_table.txt"), destination_file)
      except:
        print(f"error: unable to move symbol table to {destination_file}.")
        return 1

  print("generation successfully completed, exiting ...")
  return 0


if __name__ == "__main__":
  # command line arguments processing
  epilog = "note : if you specify neither '-a' nor '-x' both will be generated and moved into destination folder"
  parser = argparse.ArgumentParser(epilog=epilog)
  parser.add_argument("-o", help="experiment folder", dest="res_folder", metavar="path", action="store", required=True)
  parser.add_argument("-a", help="only copy to destination generated architecture description files", dest="arch_desc_only", action="store_true")
  parser.add_argument("-x", help="only copy to destination generated executables", dest="exec_only", action="store_true")
  args = parser.parse_args()
  return_val = generate_executable(**vars(args))
  sys.exit(return_val)