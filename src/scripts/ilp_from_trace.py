#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from os import path, sched_getaffinity
from datetime import datetime
import numpy as np
import sys
import argparse

from docplex.mp.model import Model, Context

FAST = 0
SLOW = 1

parentfolder = path.dirname(__file__)
cpu_count = len(sched_getaffinity(0)) or 4

def ilp_from_trace(verbose=False, name="noname.placement", alloc_log_file_name=None, mem_arch_file_name=None, ilp=True):
  log_output = ""
  log_output += ("Placement problem offline solving for Dycton\n")
  log_output += ("====================================================================================================\n")
  log_output += ("author: T. Delizy (delizy.tristan@gmail.com)\n")
  log_output += str(datetime.now())
  
  if mem_arch_file_name is None or not path.exists(mem_arch_file_name):
    log_output += ("error: missing memory architecture description file.\n")
    print(log_output)
    return 1
  elif alloc_log_file_name is None or not path.exists(alloc_log_file_name):
    log_output += ("error: missing allocation log file.\n")
    print(log_output)
    return 1

  if verbose:
    log_output += ("====================================================================\n")
    log_output += ("parsing memory architecture description file\n")
    log_output += ("====================================================================\n")
  
  mem_arch = np.loadtxt(mem_arch_file_name, delimiter=':', dtype=int, usecols=range(1, 5)).tolist()

  if len(mem_arch) > 2:
    log_output += ("error: this script doesn't support yet more than 2 memory banks.\n")
    log_output += str(mem_arch)
    print(log_output)
    return 1

  m0, m1 = mem_arch # M0 == Fast, M1 == Slow
  m0_size, m0_readlatency, m0_writelatency = m0[1], m0[2], m0[3]
  m1_size, m1_readlatency, m1_writelatency = m1[1], m1[2], m1[3]

  if verbose:
    log_output += (f"memory architectures containing {len(mem_arch)} heaps.\n")
    log_output += (f"{mem_arch}\n")

  if verbose:
    log_output += ("====================================================================\n")
    log_output += ("parsing allocation log\n")
    log_output += ("====================================================================\n")
  # sorted by request date (generated in that order)
  alloc_log = np.loadtxt(alloc_log_file_name, delimiter=';', dtype=int, converters={8: lambda s: int(s, 16)}, usecols=range(9)).tolist()

  #addr;size;malloc_date_cycles;lifespan_cycles;r_count;w_count;alloc_order;free_order;alloc_site;fallback

  objects = []
  # score_computation = []

  for index, alloc in enumerate(alloc_log) :
    # gather allocation infos
    size = alloc[1]
    lifespan_cycles = alloc[3]
    r_count = alloc[4]
    w_count = alloc[5]
    alloc_order = alloc[6]
    free_order = alloc[7]
    alloc_site = alloc[8]

    #computing score for greedy offline heuristic (if no ilp)
    obj_score = 0
    if not ilp and size * lifespan_cycles != 0:
      overlaping_allocations = list(filter(lambda x: alloc_order <= x[6] < free_order, alloc_log))
      malloc_overlap = 1 + len(overlaping_allocations)
      obj_score = float(r_count * (m1_readlatency - m0_readlatency) + w_count * (m1_writelatency - m0_writelatency)) / float(size * malloc_overlap)
      # access frequency per byte : sum of read and writes of an object times latency delta (respectively for reads and writes)
      # divided by the size of the object and the overlapping allocation (1 + objects allocated during considered object lifespan count)
      # score_computation.append(obj_score)
    
    # construct allocated object descriptor
    if ilp:
      obj_desc = [index, size, alloc_order, free_order, r_count, w_count, 0, 2, alloc_site]
    else:
      obj_desc = [index, size, alloc_order, free_order, r_count, w_count, obj_score, 3, alloc_site]
    # add to array
    objects.append(obj_desc)

  obj_count = len(objects)


  #computing score for greedy offline heuristic (if no ilp)
  # by default we use an ILP to decide what object should be in the fast memory
  # this require to select interresting big objects from heap trace, construct a ilp problem from them
  # solve it (curretly using lp-solve) and reimpact placement decision to objects allocation
  if ilp:
    c = Context.make_default_context()
    c.cplex_parameters.threads = cpu_count
    m = Model(name="ilp_from_trace", context=c)

    if verbose:
      log_output += ("====================================================================\n")
      log_output += ("objective function\n")
      log_output += ("====================================================================\n")
    
    minimization_expr = m.linear_expr()
    for idx, obj in enumerate(objects):
      variable = m.binary_var(name=f"x{idx}")
      read_count, write_count = obj[4], obj[5]
      coef = read_count * (m1_readlatency - m0_readlatency) + write_count * (m1_writelatency - m0_writelatency)
      minimization_expr.add_term(variable, -coef)
    m.minimize(minimization_expr)

    if verbose:
      log_output += ("====================================================================\n")
      log_output += ("object variable constraints\n")
      log_output += ("====================================================================\n")

    if verbose:
      log_output += ("====================================================================\n")
      log_output += ("temporal checking constraints\n")
      log_output += ("====================================================================\n")
      log_output += ("alloc " + ", ".join(map(lambda x: x[2], objects)) + "\n")
      log_output += ("frees " + ", ".join(map(lambda x: x[3], objects)) + "\n")
    
    considered_allocs = []
    for obj in objects:
      previous_memory_interaction_id = obj[3] - 1
      previous_memory_interaction_info = next(filter(lambda x: x[2] == previous_memory_interaction_id, objects), None)
      if previous_memory_interaction_info is not None:
        considered_allocs.append(previous_memory_interaction_info)
  
    considered_allocs_length = len(considered_allocs)
    if verbose:
      log_output += (f"end of loop, considered allocs count: {considered_allocs_length}\n")

    for c_idx, alloc in enumerate(considered_allocs):
      alloc_order = alloc[2]
      alive_objects = list(filter(lambda x: x[2] <= alloc_order <= x[3], objects))
      constraints_expr = m.linear_expr()
      for idx, obj in enumerate(alive_objects):
        index, size = obj[0], obj[1]
        constraints_expr.add_term(m.get_var_by_name(f"x{index}"), size)
      m.add_constraint(constraints_expr <= m0_size)

      if verbose and c_idx % (1 + considered_allocs_length / 100) == 0:
        log_output += (f"constraint {c_idx * 100 / considered_allocs_length}%\n")

    if verbose:
      log_output += ("====================================================================\n")
      log_output += ("calling solver on problem.lp\n")
      log_output += ("====================================================================\n")

    solution = m.solve()

    # then retrieveing result and reimpacting it in the objects
    log_output += ("printing solutions:\n")
    ilp_values = list(map(int, solution.get_all_values()))
    log_output += (f"ilp return values :\n {ilp_values}\n")
    for idx in range(len(objects)):
      objects[idx][7] = FAST if ilp_values[idx] == 1 else SLOW

  else:
    log_output += ("====================================================================\n")
    log_output += ("Greedy offline solution: heap filling\n")
    log_output += ("====================================================================\n")
    size_limit = [0] * (len(objects) * 2)

    # try adding objects in fast memory in decreasing score order, until memory is "full"
    for obj in sorted(objects, key=lambda obj: obj[6], reverse=True):

      idx, size, alloc_order, free_order, status = obj[0], obj[1], obj[2], obj[3], obj[7]

      # don't consider already allocated objects
      if status!= 3:
        continue

      # try to add it in fast memory
      section = size_limit[alloc_order:free_order]
      if (max(section) + size) > m0_size:
        objects[idx][7] = SLOW
      else:
        objects[idx][7] = FAST
        for i in range(alloc_order, free_order):
          size_limit[i] += size

  log_output += ("* * * * * * * object repartition:\n")
  log_output += (f"\tin fast heap: {len(list(filter(lambda x: x[7] == FAST, objects)))}\n")
  log_output += (f"\tin slow heap: {len(list(filter(lambda x: x[7] == SLOW, objects)))}\n")
  log_output += (f"\tnot processed (should be 0!): {len(list(filter(lambda x: x[7] == 2, objects)))}\n")
  log_output += (f"\tundecidable: {len(list(filter(lambda x: x[7] == 3, objects)))}\n")

  sorted_objects = sorted(objects, key=lambda obj: obj[2])

  with open(name, "w") as oracle_file:
    oracle_buffer = ""
    for obj in sorted_objects:
      oracle_buffer += f"{int(obj[7])}\n"
    oracle_file.write(oracle_buffer)

  if verbose:
    log_output += ("script end, returning.\n")
  print(log_output)
  return 0


if __name__ == "__main__":
  # command line arguments processing
  epilog = ("Generates and solve ilp problem from a given reference execution using cplex\n"
            "(cplex recommended for programs that have more than 1000 objects...)\n"
            "Also generates greedy offline solution based on access frequency per byte.\n"
            "CAUTION : work only for 2 memory banks !")
  parser = argparse.ArgumentParser(epilog=epilog)
  parser.add_argument("-V", help="activate verbosity", dest="verbose", action="store_true")
  parser.add_argument("-i", help="input heap object file path", metavar="input", dest="alloc_log_file_name", action="store", required=True)
  parser.add_argument("-a", help="memory architecture file path", metavar="mem_arch", dest="mem_arch_file_name", action="store", required=True)
  parser.add_argument("-n", help="output oracle file name", metavar="name", dest="name", action="store", default="noname.placement")
  parser.add_argument("--no_ilp", help="greedy offline solution based on access frequency per byte", dest="ilp", action="store_false")
  args = parser.parse_args()
  return_val = ilp_from_trace(**vars(args))
  sys.exit(return_val)

