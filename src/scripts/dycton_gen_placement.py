#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from datetime import datetime
from os import path, mkdir
import argparse
import sys
import re

parentfolder = path.dirname(__file__)

sys.path.append("../../")
sys.path.append(parentfolder)

from ilp_from_trace import ilp_from_trace

def ilp_from_trace_wrapper(desc, name, sw, hw, dstring, xp_folder, res_folder, ilp=True):
  # ILP offline placement
  filename = f"{sw}_arch{hw}{dstring}_{name}.placement"
  ret = ilp_from_trace(
    alloc_log_file_name=path.join(xp_folder, f"ref_exec/{sw}/{dstring}/heap_objects.log"),
    mem_arch_file_name=path.join(xp_folder, "archi_desc", desc),
    name=path.join(res_folder, filename),
    ilp=ilp
  )
  if ret != 0:
    print(f"error in {name} placement construction")
    return ret
  return 0

# offline placement strategy generation
def generate_placement(xp_folder=None):
  # start by printing the date
  date = str(datetime.now())
  print("Placement for heterogeneous dynamic memory allocation offline solving for Dycton")
  print("====================================================================================================")
  print("author: T. Delizy (delizy.tristan@gmail.com)")
  print(date)


  if xp_folder is None:
    date = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    res_folder = path.join(parentfolder, f"offline_placement_generation_{date}")
  else:
    res_folder = path.join(xp_folder, "offline_placements")

  try:
    mkdir(res_folder)
  except:
    print("unable to create res folder.")
    return 1

  try:
    import xp_config as xp
    print("config file imported.")
  except:
    print("config import failed.")
    return 1

  print("target_sw", xp.target_sw)
  print("target_hw", xp.target_hw)
  print("target_strats", xp.target_strats)

  # if target strategies includes profile-based strategies, we need to solve ILP on the other half of datasets to generates ilp profiles
  if any(x in xp.target_strats for x in ["profile"]):
    considered_datasets = [0, 1, 2, 3, 4, 5, 6, 7]
  else:
    considered_datasets = xp.target_datasets

  ilp_percentages = set((int(x.split("_")[1]) for x in xp.target_strats if re.match(r"(^ilp_\d{1,2}$)", x)))
  density_percentages = set((int(x.split("_")[1]) for x in xp.target_strats if re.match(r"(^density_100$)|(^density_\d{1,2}$)", x)))
  # build offline placement solutions
  for sw in xp.target_sw:
    for hw in xp.target_hw:
      if hw not in ["0", "100"]:
        for d in considered_datasets:
          dstring = f"_d{d}"

          descriptions = [f"{sw}_arch{hw}{dstring}_100p.desc"] if any(x in xp.target_strats for x in ["ilp_100", "ilp_upper_bound"]) else []
          descriptions += [f"{sw}_arch{hw}{dstring}_{percent}p.desc" for percent in ilp_percentages]
          descriptions += [f"{sw}_arch{hw}{dstring}_{percent}p.desc" for percent in density_percentages]

          names = ["ilp_100p"] if any(x in xp.target_strats for x in ["ilp_100", "ilp_upper_bound"]) else []
          names += [f"ilp_{percent}p" for percent in ilp_percentages]
          names += [f"density_{percent}p" for percent in density_percentages]

          # Generate arguments
          for desc, name in zip(descriptions, names):
            print("building offline solution for", sw, "on architecture", hw, "targetting dataset", d, "with strategy", name[:-1])
            ret = ilp_from_trace_wrapper(desc, name, sw, hw, dstring, xp_folder, res_folder, bool(re.match(r"(^ilp_100p$)|(^ilp_\d{1,2}p$)", name)))
            if ret != 0:
              return ret
    
  print("placement generation done.")
  return 0

if __name__ == "__main__":
  # command line arguments processing
  epilog = ("this script should not be called directly but throug the experiment preparation script\n"
            "it relies on the ../../xp_config.py file and generates the needed placement files from\n"
            "references executions for the whole experiments (calls ilp_from_trace.py)\n"
            "its only parameter is the path to the experiment folder being generated")
  parser = argparse.ArgumentParser(epilog=epilog)
  parser.add_argument("xp_folder", help="experiment folder", action="store")
  args = parser.parse_args()
  return_val = generate_placement(**vars(args))
  sys.exit(return_val)