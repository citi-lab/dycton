#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import matplotlib.pyplot as plt
import matplotlib.patches as patches

from datetime import datetime
from os import path, mkdir
import argparse
import csv
import sys

# plot visual scheme definition
# c = color
# m = marker
# l = label
plot_config = { "baseline":{"c":'blue', "m":'v', "l":'Fast First', "ls":"-"},
                "ilp_upper_bound":{"c":'black', "m":'*', "l":'Upper Bound', "ls":"--"},
                "ilp":{"c":'red', "m":'o', "l":'ILP', "ls":"-"},
                "den":{"c":'green', "m":'d', "l":'Density', "ls":"-"},
                "profile":{"c":'darkorange', "m":'x', "l":'Alloc Site Profile', "ls":"-"}}

def result_plot(input_csv, paper_mode, graphics=False):
  print("Dycton plot script")
  print("================================================================================")
  print("author: T. Delizy (delizy.tristan@gmail.com)")
  print(str(datetime.now()))
  print("")

  if input_csv is None or not path.exists(input_csv):
    print("error: invalid path.")
    return 1

  print("================================================================================")
  print("data retrieving and preparation")
  print("================================================================================")

  results = {}
  ignored_experiences = []

  print("> populating result structure... ", end=' ')
  with open(input_csv, newline="") as csvfile:
    data_reader = csv.DictReader(csvfile)
    for row in data_reader:
      if row["Return Code"] != "0":
        ignored_experiences.append(row['Experience'])
        continue
      splitted_xp = row["Experience"].split("_")
      offset = 0
      for i, s in enumerate(splitted_xp):
        if "arch" in s: offset = i - 1
      sw_name = splitted_xp[0]
      hw_cursor = splitted_xp[1 + offset][4:]
      ds_cursor = splitted_xp[2 + offset][1:]
      # strat_cursor = splitted_xp[3 + offset]
      # strat_info = splitted_xp[4 + offset] if "run" != splitted_xp[4 + offset] else None
      strat_name = "_".join(splitted_xp[3 + offset:-1])
      if "profile" in strat_name:
        strat_name = "profile"
      if not sw_name in results:
        results[sw_name] = {hw_cursor: {strat_name: {ds_cursor: int(row["Final Cycles"])}}}
      elif not hw_cursor in results[sw_name]:
        results[sw_name][hw_cursor] = {strat_name: {ds_cursor: int(row["Final Cycles"])}}
      elif not strat_name in results[sw_name][hw_cursor]:
        results[sw_name][hw_cursor][strat_name] = {ds_cursor: int(row["Final Cycles"])}
      elif not ds_cursor in results[sw_name][hw_cursor][strat_name]:
        results[sw_name][hw_cursor][strat_name][ds_cursor] = int(row["Final Cycles"])
      elif strat_name == "profile" and results[sw_name][hw_cursor][strat_name][ds_cursor] > int(row["Final Cycles"]):
        results[sw_name][hw_cursor][strat_name][ds_cursor] = int(row["Final Cycles"])
  print("done.\n") 

  for experience in ignored_experiences:
    print(f"Ignored experience : {experience} (invalid return code)")

  if not path.exists("graphs"):
    try:
      mkdir("graphs")
    except:
      print("error: failed to create graphs directory")
      return 1

  for sw in results:
    print("================================================================================")
    print("plotting", sw)
    print("================================================================================", "\n\n")

    strats = []
    arch = list(map(int, results[sw].keys()))
    arch_names = [f"{a}%" for a in arch]
    y_min = 100
    y_max = 0
    for hw in results[sw]:
      if hw != "0" and hw != "100":
        strats = results[sw][hw].keys()

    print("arch = ", results[sw].keys())
    print("arch names = ", arch_names)
    print("strats =", list(strats))


    # create plot
    script_dpi = 80
    if paper_mode:
      f, ax = plt.subplots(figsize=(500/script_dpi, 350/script_dpi), dpi=script_dpi)
      line_w = 1.2
      markersize = 7
      capsize = 4
      elw = 0.7
    else:
      f, ax = plt.subplots(figsize=(1000/script_dpi, 700/script_dpi), dpi=script_dpi)
      line_w = 2
      markersize = 8
      capsize = 8
      elw = 1
    # plot strats
    # results[cur_sw][cur_hw][cur_strat][cur_ds]
    ilp_strats = []
    den_strats = []
    adjustment_x = -0.4
    adjustment_x_val = 0.2
    for s in strats:
      x = []
      y = {"avg":[], "down":[], "up":[]}
      if s in ["baseline", "ilp_upper_bound", "profile"]:
        for hw in sorted(results[sw].keys(), key=int):
          if s in results[sw][hw]:
            x.append(int(hw) + adjustment_x)
            speedups = []
            for k, v in results[sw][hw][s].items():
              speedups.append((float(results[sw]["0"]["baseline"][k]) - float(v))/float(results[sw]["0"]["baseline"][k])*100)
            avg = sum(speedups)/ float(len(speedups))
            y["avg"].append(avg)
            y["down"].append(avg - min(speedups))
            y["up"].append(max(speedups) - avg)
            if min(speedups) < y_min:
              y_min = min(speedups)
            if max(speedups) > y_max:
              y_max = max(speedups)
        eb = ax.errorbar(x, y["avg"],
          yerr=[y["down"], y["up"]],
          color=plot_config[s]["c"],
          linestyle=plot_config[s]["ls"],
          marker=plot_config[s]["m"],
          markersize=markersize,
          capsize=capsize,
          lw=line_w,
          elinewidth=elw,
          clip_on=False,
          label=plot_config[s]["l"] )

        adjustment_x += adjustment_x_val

      elif "ilp" in s:
        ilp_strats.append(s)
      elif "den" in s:
        den_strats.append(s)

    if len(ilp_strats) != 0:
      x = []
      y = {"avg":[], "down":[], "up":[]}
      for hw in sorted(results[sw].keys(), key=int):
        # print "HW=", hw
        if hw == "0" or hw == "100":
          continue
        print(f"on arch {hw}")
        speedups = []
        
        for d in results[sw][hw][ilp_strats[0]].keys():
          percentages = []
          for s in ilp_strats:
            try:
              percentages.append((float(results[sw]["0"]["baseline"][d]) - float(results[sw][hw][s][d]))/float(results[sw]["0"]["baseline"][d])*100)
            except:
              pass
          val = max(percentages)
          idx = percentages.index(val)
          print(f"choosing ilp strategy '{ilp_strats[idx]}' for dataset '{d}'")
          speedups.append(val)
        x.append(int(hw)+ adjustment_x)
        avg = sum(speedups)/ float(len(speedups))
        y["avg"].append(avg)
        y["down"].append(avg - min(speedups))
        y["up"].append(max(speedups) - avg)
        if min(speedups) < y_min:
          y_min = min(speedups)
        if max(speedups) > y_max:
          y_max = max(speedups)
      eb = ax.errorbar(x, y["avg"],
        yerr=[y["down"], y["up"]],
        color=plot_config["ilp"]["c"],
        linestyle=plot_config["ilp"]["ls"],
        marker=plot_config["ilp"]["m"],
        markersize=markersize,
        capsize=capsize,
        lw=line_w,
        elinewidth=elw,
        clip_on=False,
        label=plot_config["ilp"]["l"])

      adjustment_x += adjustment_x_val


    if len(den_strats) != 0:
      x = []
      y = {"avg":[], "down":[], "up":[]}
      for hw in sorted(results[sw].keys(), key=int):
        # print "HW=", hw
        if hw == "0" or hw == "100":
          continue
        print(f"on arch {hw}")
        speedups = []
        for d in results[sw][hw][den_strats[0]].keys():
          percentages = []
          for s in den_strats:
            try:
              percentages.append((float(results[sw]["0"]["baseline"][d]) - float(results[sw][hw][s][d]))/float(results[sw]["0"]["baseline"][d])*100)
            except:
              pass
          val = max(percentages)
          idx = percentages.index(val)
          print(f"choosing density strategy '{den_strats[idx]}' for dataset '{d}'")
          speedups.append(val)
        x.append(int(hw) + adjustment_x)
        avg = sum(speedups)/ float(len(speedups))
        y["avg"].append(avg)
        y["down"].append(avg - min(speedups))
        y["up"].append(max(speedups) - avg)
        if min(speedups) < y_min:
          y_min = min(speedups)
        if max(speedups) > y_max:
          y_max = max(speedups)
      eb = ax.errorbar(x, y["avg"],
        yerr=[y["down"], y["up"]],
        color=plot_config["den"]["c"],
        linestyle=plot_config["den"]["ls"],
        marker=plot_config["den"]["m"],
        markersize=markersize,
        capsize=capsize,
        lw=line_w,
        elinewidth=elw,
        clip_on=False,
        label=plot_config["den"]["l"])

      adjustment_x += adjustment_x_val

    for a in arch:
      ax.plot([a,a] , [-100, 100], lw=0.5, linestyle='--',color='grey', zorder=0)

    plt.xticks(arch, arch_names)
    ax.set_xlim([-0.7, 100.7])
    ax.set_ylim([y_min - 0.5, y_max + 0.5])

    ax.tick_params(axis='both', which='major', labelsize=11)


    handles, labels = ax.get_legend_handles_labels()
    if len(results[sw]["0"]["baseline"].keys()) > 1:
      extraString = "(Average,\nmin and max)"
      handles.append(patches.Patch(color='none', label=extraString))
      ax.legend(handles=handles)


    if paper_mode:
      ax.legend(loc='lower right', bbox_to_anchor=(1, 0), ncol=2, fontsize=11, handles=handles)
      ax.set_xlabel('Fraction of heap in fast memory', fontsize=13)
      ax.set_ylabel('Execution Time Reduction (%)', fontsize=13)
      title_sz = 13
    else:
      ax.legend(loc='lower right', bbox_to_anchor=(1, 0.04), ncol=2, fontsize=14, handles=handles)
      ax.set_xlabel('Fraction of heap in fast memory', fontsize=16)
      ax.set_ylabel('Execution Time Reduction (%)', fontsize=16)
      title_sz = 16



    if sw == "json_parser":
      ax.set_title("json", fontsize=title_sz)
    else:
      ax.set_title(sw, fontsize=title_sz)


    f.tight_layout()

    strat_string = ""
    dataset_string = ""
    arch_string = ""

    for s in strats:
      if s == "baseline":
        strat_string += "ff-"
      elif s == "ilp":
        strat_string+= "ilp-"
      elif s == "density":
        strat_string+= "density-"
      elif s == "profile":
        strat_string+= "prof-"
    strat_string = strat_string[0:-1]

    arch_string = "-".join(results[sw].keys())

    dataset_string = "-".join(results[sw]["0"]["baseline"].keys())


    plt.savefig(path.join("graphs", f"{sw}_A({arch_string}D({dataset_string}S({strat_string}).pdf"), dpi=330)


    # show it to the world
    if graphics:
      plt.show()


  return 0

if __name__ == "__main__":
  # command line arguments processing
  epilog = "plotting whats described in the xp_config.py in current directory, fallback to the xp_config.py present in .. otherwise."
  parser = argparse.ArgumentParser(epilog=epilog)
  parser.add_argument("input_csv", help="input results csv file path", action="store")
  parser.add_argument("-p", dest="paper_mode", help="paper output (smaller image size, increased text size)", action="store_true")
  parser.add_argument("--nographics", dest="graphics", help="toggle off plots rendering", action="store_false")
  args = parser.parse_args()
  return_val = result_plot(**vars(args))
  sys.exit(return_val)