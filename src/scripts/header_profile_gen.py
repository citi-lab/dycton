#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory 
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from datetime import datetime
from os import path, listdir
import numpy as np
import argparse
import sys

alloc_and_fallback = "if(l < prof_len){\\\n\
\talloc_context = &(multi_heap_ctx[0]);\\\n\
\treturnPointer = newlib_malloc_r(_impure_ptr, sz);\\\n\
\tif (!returnPointer) {\\\n\
\t\tALLOC_INTERNAL_FAIL(0);\\\n\
\t\talloc_context = &(multi_heap_ctx[1]);\\\n\
\t\treturnPointer = newlib_malloc_r(_impure_ptr, sz);\\\n\
\t\tif (!returnPointer) {\\\n\
\t\t\texit(36);\\\n\
\t\t}\\\n\
\t}\\\n\
}else{\\\n\
\talloc_context = &(multi_heap_ctx[1]);\\\n\
\treturnPointer = newlib_malloc_r(_impure_ptr, sz);\\\n\
\tif (!returnPointer) {\\\n\
\t\tALLOC_INTERNAL_FAIL(0);\\\n\
\t\talloc_context = &(multi_heap_ctx[0]);\\\n\
\t\treturnPointer = newlib_malloc_r(_impure_ptr, sz);\\\n\
\t\tif (!returnPointer) {\\\n\
\t\t\texit(36)\\\n;\
\t\t}\\\n\
\t}\\\n\
}\\\n\
}while(0)"

app = ""

parentfolder = path.dirname(__file__)

def generate_header_profile(profile_path=None):
  global app

  date_start = str(datetime.now())
  profiles = []

  print("Allocation site profile header generation for Dycton")
  print("====================================================================================================")
  print("author: T. Delizy (delizy.tristan@gmail.com)")
  print(date_start)

  if profile_path is None or not path.exists(profile_path):
    print("error: missing profile folder.\n")
    return 1

  for filepath in listdir(profile_path):
    fpath = path.join(profile_path, filepath)
    if ".profile" not in filepath:
      print("what is", fpath, "(ignored)")
    else:
      profiles.append(fpath)
  
  if len(profiles) == 0:
    print("you must provide input profile file, exiting...")
    return 1

  if path.basename(profile_path) in ["json_parser", "dijkstra", "jpg2000", "h263", "ecdsa", "jpeg", "dummy_memset"]:
    app = path.basename(profile_path)
  else:
    print("application not recognized, see header_profile_gen.py")
    return 1

  profile_header = f"{app}_alloc_site_profile.h"
  
  print("\n\ttarget application:", app)
  print("\n\toutput header:", profile_header)
  print("\n\tinput profiles (", len(profiles), ")")
  print(profiles)
  print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n")

  profile_max_len = 0
  alloc_site_dict = {}

  print("> Reading input profiles into a dict...", end=" ", flush=True)
  # 4196644 2.668425558610061 15.972159613408104  (get_quoted_string)
  for p in profiles:
    with open(p, "r") as infile:
      for line in infile:
        ad, den = line.split("\t")[:2]
        den = float(den)
        if ad in alloc_site_dict:
          alloc_site_dict[ad]["densities"].append(den)
        else:
          alloc_site_dict[ad] = {"densities": [den], "metric": 0}

  profile_max_len = len(alloc_site_dict)

  print("done.")

  print("> Aggregate profiles", end=" ", flush=True)
  # # max density on different datasets profiles
  # print("metric: max...",)
  # for k, v in alloc_site_dict.iteritems():
  #   v["metric"] = np.max(v["densities"])

  # # median density on different datasets profiles
  # print("metric: median...",)
  # for k, v in alloc_site_dict.iteritems():
  #   v["metric"] = np.median(v["densities"])

  # # average density on different datasets profiles
  # print("metric: average...",)
  # for k, v in alloc_site_dict.iteritems():
  #   v["metric"] = np.average(v["densities"])

  # max density on different datasets profiles
  print("metric: 90 percentile...", end=" ", flush=True)
  for key in alloc_site_dict:
    alloc_site_dict[key]["metric"] = np.percentile(alloc_site_dict[key]["densities"], 90)
  print("done.")

  print("\tmax length of profile :", profile_max_len)
  print("\tordered aggregated alloc site frequencies per byte:")

  sorted_alloc_site_array = sorted(alloc_site_dict.items(), key=lambda values: values[1]["metric"], reverse=True)
  for site in sorted_alloc_site_array:
    ad, ad_info = site
    print(f"\t {ad} : {ad_info['metric']}")

  print("> Writing result into header file...", end=" ", flush=True)
  # PREAMBLE ======================================================================================
  # write header
  with open(path.join(profile_path, profile_header), "w") as outfile:
    
    # static preamblule
    loglines = "//automatically generated during each build, please see src/scripts/header_profile_gen.py\n"
    # define addresses
    loglines += "".join(f'#define __APP_ALLOC_SITE_{i} ({site[0]})\n' for i, site in enumerate(sorted_alloc_site_array))

    loglines += (f'#define __APP_ALLOC_SITE_LENGTH ({profile_max_len})\n\n' # define the max profile length
              "// #define PROFILE_GARDS\n" # define guards (deactivated by default)
              "#ifdef PROFILE_GARDS\n"
                "\t#define ASSERT_PROFILE_CONSISTENCY(a) do{ if(!("
              f'{" || ".join(f"a == __APP_ALLOC_SITE_{i}" for i in range(profile_max_len))}'
              ')){print("ERROR : caller @ not in profile !!");exit(a);} }while(0)\n'
              "#else\n"
                "\t#define ASSERT_PROFILE_CONSISTENCY(a) do{}while(0)\n"
              "#endif\n\n")


# ALLOCATION SITE PROFILE =======================================================================
    # define switch case macro
    loglines += ("#define GENERATED_SWITCH_PROFILE(addr, prof_len) do { \\\n"
                "uint32_t l = 0xFFFF;\\\n"
                "switch (addr) {\\\n")

    loglines += "".join(f"case __APP_ALLOC_SITE_{i}:\\\n\tl = {i};\\\n\tbreak;\\\n"
                        for i in range(profile_max_len))

    #switch end and check for profile length, macro static end
    loglines += f"{'}'}\\\n{alloc_and_fallback}\n\n"

    outfile.write(loglines)

  print("done.")
  print("> End of script")
  return 0

if __name__ == "__main__":
  # command line arguments processing
  epilog = ("pass profile folder to aggregate to generate header\n"
            "caution : profiles names containing \"ilp\" will be used to enhance profile and generates ilp-profile\n"
            "only files.profiles taken into account, but all of them, no check on xp_config")
  parser = argparse.ArgumentParser(epilog=epilog)
  parser.add_argument("profile_path", help="profile folder path", metavar="profile", action="store")
  args = parser.parse_args()
  return_val = generate_header_profile(**vars(args))
  sys.exit(return_val)