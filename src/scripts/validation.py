#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory 
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from os import path
import numpy as np
import sys
import argparse

def validate(log1, log2):
  date_start = str(datetime.now())

  print("validating that heap object log from 2 different")
  print("execution have taken the same placement decisions.")
  print("==================================================")
  print("author: T. Delizy (delizy.tristan@gmail.com)")
  print(date_start)

  if log1 is None or not path.exists(log1):
    print("error: missing first log file.")
    return 1
  elif log2 is None or not path.exists(log2):
    print("error: missing second log file.")
    return 1

  print("heap log 1:", log1)
  print("heap log 2:", log2)
  print("- - - - - - - - - - - - - - - - - - - - - - - - - ")

# log parsing 
  objects_log1 = np.loadtxt(log1, delimiter=';', dtype=int, converters={8: lambda s: int(s, 16)})
  objects_log2 = np.loadtxt(log2, delimiter=';', dtype=int, converters={8: lambda s: int(s, 16)})

#addr;size;malloc_date_cycles;lifespan_cycles;r_count;w_count;alloc_order;free_order;alloc_site
  trim1 = objects_log1[:,[0,1,4,5,6,7,8]]
  trim2 = objects_log2[:,[0,1,4,5,6,7,8]]

  ret = np.array_equal(trim1,trim2)

  print("are the heap logs the same ?", ret)

  if ret == False:
    print("Differences detected:")
    print("LOG1 :")
    print(trim1)
    print("LOG2 :")
    print(trim2)

  print("done. exiting...")
  return 0
  
if __name__ == "__main__":
  # command line arguments processing
  epilog = ("this scripts check that two execution heap logs are equivalent\n"
            "meaning that the same objects (same size, same alloc site)\n"
            "have been allocated and freed in the same order, and accessed\n"
            "the same number of time (R/W)")
  parser = argparse.ArgumentParser(epilog=epilog)
  parser.add_argument("log1", help="first execution heap logs", metavar="log_file", action="store")
  parser.add_argument("log2", help="second execution heap logs", metavar="log_file", action="store")
  args = parser.parse_args()
  return_val = validate(**vars(args))
  sys.exit(return_val)
