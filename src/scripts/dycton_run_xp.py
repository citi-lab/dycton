#!/usr/bin/env python3


# This file is part of the Dycton project scripts.
# This software aims to provide an environment for Dynamic Heterogeneous Memory
# Allocation for embedded devices study.

# Copyright (C) 2019  Tristan Delizy, CITI Lab, INSA de Lyon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from os import path, mkdir, cpu_count
from shutil import copytree, copyfile, rmtree
import argparse
import sys
import subprocess
from time import sleep
import numpy as np
import re

# ATTENTION : ABSOLUTE PATHS HERE !!!
# ALSO ATTENTION : PATHS END WITH / !!!

# xp paths
root_res = "results_raw"
root_exec = "xp_run"
xp_logs = path.join(root_res, "logs")

cpus = cpu_count() - 1

nice_arg = "10" # a bit nice (useless for local execution)

# get xp configuration

try:
  import xp_config as xp
  print("config file imported.")
except:
  print("config import failed.")
  sys.exit(1)

percentages = set((int(x.split("_")[1]) for x in xp.target_strats if re.match(r"(^ilp_100$)|(^ilp_\d{1,2}$)|(^density_100$)|(^density_\d{1,2}$)", x)))

def prepare_run(sw, hw, dataset, strat, length=0):
  if length > 0:
    run_name = f"{sw}_arch{hw}_d{dataset}_{strat}_{length}_run"
  else:
    run_name = f"{sw}_arch{hw}_d{dataset}_{strat}_run"
  run_path = path.join(root_exec, run_name)

  # setup clean exec environment
  try:
    if not path.exists(run_path):
      copytree("clean_env", run_path)
  except:
    print(f"error in clean environment setup for xp {run_name}.")
    return 1

  # setup embedded software
  sim_a_out = path.join(run_path, "software/a.out")
  try:
    if not path.exists(sim_a_out):
      copyfile(f"emb_softs/{sw}_soft.out", sim_a_out)
  except:
    print("error in copying embedded software.")
    return 1

  # elaborate simulator command line
  cmd_line = ["nice", "-n", nice_arg, f"../../../simulators/{sw}_sim.x", "-c", "-a"]
  if strat in ["profile"]:
    cmd_line.extend([hw, "-d", str(dataset), "-s", strat, "-p", str(length if length > 0 else xp.profile_static_opt[sw][hw])])
  elif re.match(r"(^ilp_100$)|(^ilp_\d{1,2}$)|(^density_100$)|(^density_\d{1,2}$)", strat):
    cmd_line.extend([hw, "-d", str(dataset), "-soracle", f"-f../../../offline_placements/{sw}_arch{hw}_d{dataset}_{strat}p.placement"])
  elif strat in ["ilp_upper_bound"]:
    cmd_line.extend(["-2", "-d", str(dataset), "-soracle", f"-f../../../offline_placements/{sw}_arch{hw}_d{dataset}_ilp_100p.placement"])
  else:
    cmd_line.extend([hw, "-d", str(dataset)])

  # return dict describing the run
  return {'name' : run_name, 'cmd' : cmd_line, 'path': run_path}




def run_experimentation():
  # start by printing the date
  date_start = str(datetime.now())
  print("Dycton experiment run script")
  print("================================================================================")
  print("author: T. Delizy (delizy.tristan@gmail.com)")
  print(date_start)

  try:
    if not path.exists(root_exec):
      mkdir(root_exec)
    if not path.exists(root_res):
      mkdir(root_res)
    if not path.exists(xp_logs):
      mkdir(xp_logs)
  except:
    print("error in initial folders setup.")
    return 1

  run_list_by_app = {"json_parser": [], "dijkstra": [], "jpg2000": [], "h263": [], "ecdsa": [], "jpeg": [], "dummy_memset": []}
  queue = []

  # iterate on the different application sequentially to avoid taking to much ressources
  for sw in xp.target_sw:
    # setup :
    # - creates an environment for every run of the target app / arch / strat (and multiple for profile strategies exploration)
    # - populates a dict describing each run with run name, execution path and simulator command line
    for hw in xp.target_hw:
      for d in xp.target_datasets:
        if hw in ["0", "100"] and "baseline" in xp.target_strats:
          # only execute baseline on singe heap architectures
          run_info = prepare_run(sw, hw, d, "baseline")
          if run_info == 1:
            return 1
          run_list_by_app[sw].append(run_info)
        else:
          for strat in xp.target_strats:
            # we want to launch multiple runs with the same sw, hw and strat and with different profile length
            if strat in ["profile"] and xp.profile_explo == True:
              # get the profile max length for this application
              max_len = 0
              profile_len_list = []
              with open(f"profiles/{sw}/{sw}_alloc_site_profile.h") as profile:
                for line in profile:
                  if "__APP_ALLOC_SITE_LENGTH" in line:
                    max_len = int(line.split("__APP_ALLOC_SITE_LENGTH")[1].strip(' ()\t\n\r'))
              if xp.profile_high_resolution :
                # every profile length for low values (< 20)
                profile_len_list = np.arange(1, min(20, max_len + 1), 1)
                if max_len > 20:
                  # ten profile lengths after that
                  interval = max(1, float(max_len - 20) / 10)
                  end_of_list = np.around(np.arange(20, max_len - 1, interval)).astype(int)
                  profile_len_list = np.concatenate((profile_len_list, end_of_list), 0)
              else:
                # 10 profile lengths tested
                interval = max(1, float(max_len) / 10)
                profile_len_list = np.around(np.arange(1, max_len - 1, interval)).astype(int)
              # always plot the lmast point
              np.append(profile_len_list, max_len)
              for l in profile_len_list:
                # prepare the runs for the different profile lengths
                run_info = prepare_run(sw, hw, d, strat, l)
                if run_info == 1:
                  return 1
                run_list_by_app[sw].append(run_info)
            else:
              # prepare the run for the giventarget soft, architecture and strategy
              run_info = prepare_run(sw, hw, d, strat)
              if run_info == 1:
                return 1
              run_list_by_app[sw].append(run_info)

    print(f"Running all simulations for software '{sw}'")

    # run all the "runs" preparated for the current software target
    for run in run_list_by_app[sw]:
      with open(path.join(root_res, f"{run['name']}.result"), 'w+') as outfile:
        p = subprocess.Popen(run['cmd'], cwd=path.join(run['path'], "iss"), stdout=outfile, stderr=subprocess.STDOUT)
        run['process'] = p
        queue.append(p)
      while len(queue) >= cpus:
        indexes_to_remove = []
        for idx, proc in enumerate(queue):
          if proc.poll() != None:
            indexes_to_remove.append(idx)
        for shift, idx in enumerate(indexes_to_remove):
          queue.pop(idx - shift)
        sleep(1)
    # wait end of simulations
    for run in run_list_by_app[sw]:
      run['ret'] = run['process'].wait()
      if run['ret'] != 0:
        print(f"error in {run['name']}.")

    # retrieve logs and cleanup
    for run in run_list_by_app[sw]:
      # if specified in xp_config.py save execution logs
      if xp.save_heap_logs:
        try:
          log_path = path.join(xp_logs, run['name'])
          if not path.exists(log_path):
            copytree(path.join(run['path'], "logs"), log_path)
        except:
          print(f"error in retrieving logs for {run['name']}.")
          return 1
      if "h263" in run['name']:
        try:
          log_path = path.join(xp_logs, run['name'], "output.263")
          if not path.exists(log_path):
            copyfile(path.join(run['path'], "software/h263/datasets/output.263"), log_path)
        except:
          print(f"error in can't retrieve output.263 {run['name']}.")
          return 1

      # delete run environment
      try:
        if path.exists(run['path']):
          rmtree(run['path'])
      except:
        print(f"error in cleaning up {run['name']}.")
        return 1

  print("end of experiment run, exiting...")
  print(f"End time : {datetime.now()}")
  return 0

if __name__ == "__main__":
  # command line arguments processing
  parser = argparse.ArgumentParser()
  args = parser.parse_args()
  return_val = run_experimentation(**vars(args))
  sys.exit(return_val)
