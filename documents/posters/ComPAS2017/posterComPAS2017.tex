\documentclass[final,t]{beamer}
\mode<presentation>{\usetheme{RWSposter}}
\usepackage{ragged2e}

%==============================================================================%
% additional settings
\setbeamerfont{itemize}{size=\normalsize}
\setbeamerfont{itemize/enumerate body}{size=\normalsize}
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}

%==============================================================================%
% additional packages
\usepackage[orientation=portrait,size=a0b,scale=1.1]{beamerposter}
\usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage{exscale}
\usepackage{booktabs, array}
\usepackage{algorithm, algorithmic}
\usepackage{xspace}
\usepackage{wrapfig}
\usepackage{multicol}
\usepackage{tcolorbox}
\usepackage{minted}

\setbeamercolor{titre}{bg=inriagreen,fg=white}
\setbeamercolor{texte}{bg=inriagreen!70,fg=black}

\usemintedstyle{tango}

%==============================================================================%
\listfiles
\title{Towards Dynamic Memory Management for Normally-Off Embedded Systems}
\author{Tristan Delizy - \normalsize{ 1$^{st}$ year Ph.D. Student}}
\advisors{Advisors : Guillaume Salagnac, Kevin Marquet, Matthieu Moy, Tanguy Risset}
\partnership{Industrial Partner : eVaderis}
\addressints{}
\institutes{Univ Lyon, Inria, INSA Lyon, CITI Laboratory}
\event{ComPAS 2017}
\email{tristan.delizy@insa-lyon.fr}


%%%%%%%%%%%%%%%%%%%%
% Bibliographic references
\usepackage[
backend=bibtex,
maxnames=4,
minnames=2,
maxbibnames=10,
bibencoding=inputenc,
style=authoryear-comp,
citestyle=authoryear-comp,
sorting=anyt, % Sort by alphabetic label, name, year, title
doi=false,
isbn=false,
url=false,
uniquename=init,
]{biblatex}
\addbibresource{illustrations.bib}
% or: plain,unsrt,alpha,abbrv,acm,apalike,...

% for in text full cite
\DeclareCiteCommand{\fullcitebib}
  {\renewcommand{\finalnamedelim}
   {\ifnum\value{liststop}>2 \finalandcomma\fi\addspace\&\space}%
   \list{}
   {\setlength{\leftmargin}{\bibhang}%
     \setlength{\itemindent}{-\leftmargin}%
     \setlength{\itemsep}{\bibitemsep}%
     \setlength{\parsep}{\bibparsep}}\item}
  {\usedriver
    {}
    {\thefield{entrytype}}\finentry}
  {\item}
  {\endlist\global\undef\bbx}
\makeatother

\renewcommand*{\bibfont}{\scriptsize}
\setbeamertemplate{bibliography item}[triangle]

\makeatletter

\newrobustcmd*{\parentexttrack}[1]{%
  \begingroup
  \blx@blxinit
  \blx@setsfcodes
  \blx@bibopenparen#1\blx@bibcloseparen
  \endgroup}

\AtEveryCite{%
  \let\parentext=\parentexttrack%
  \let\bibopenparen=\bibopenbracket%
  \let\bibcloseparen=\bibclosebracket}

\makeatother

\definecolor{main}{HTML}{34676D}

\setbeamercolor{bibliography entry author}{fg=main}
\setbeamercolor{bibliography entry note}{fg=black}
\setbeamercolor{itemize item}{fg=main}

% \setbeamercolor{block body example}{bg=red!20!white}
% \setbeamercolor{block title example}{fg=red, bg=red!40!white}


\newcommand{\paragraphtitle}[1]{\begin{center}\textbf{#1}\end{center}}


%==============================================================================%
\begin{document}
%==============================================================================%
%==============================================================================%
\begin{frame}[fragile]
%==============================================================================%
% titre
%------------------------------------------------------------------------------%
% géré dans le thème
%------------------------------------------------------------------------------%
% NVRAMs | normally Off
%------------------------------------------------------------------------------%
\vspace{-.005\textheight}
\begin{columns}[T]
	\begin{column}{.493\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main,title={\begin{center}\Large{\textbf{Emerging Non-Volatile Memory}}\end{center}}]
	    \begin{table}
	     \vspace{-.012\textheight}
	       \begin{tabular}{ p{.17\textwidth}  p{.4\textwidth}  p{.4\textwidth} }
	    %     \begin{center}technology\end{center} & \begin{center}principe physique\end{center} & \begin{center}details\end{center} \\
	    %     \hline
	        \textbf{FRAM} \newline Ferroelectric~RAM & \vspace{-.024\textheight}\hspace{.05\textwidth}\includegraphics[width=.3\textwidth]{figures/Fram_hysteresis.png} & {\parbox{0.37\textwidth}{
            \begin{itemize}
              \item Charge trap
              \item Read operation is destructive
              \item 125nm limitation
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Low capacity
            \end{itemize} }} \\
	        \hline
	        \textbf{PCM} \newline Phase~Change \newline Memory & \vspace{-.018\textheight}\includegraphics[width=.4\textwidth]{figures/pcm_cristal.png} & {\parbox{0.37\textwidth}{
            \begin{itemize}
              \item Amorphous / crystalline state
              \item Variability in the SET and RESET \rlap{currents}
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Limited write endurance
            \end{itemize} }} \\
	        \hline
	        \textbf{ReRAM} \newline Resistive~RAM &  \vspace{-.024\textheight}\includegraphics[width=.4\textwidth]{figures/RERAM_Wei2013.png} & {\parbox{0.37\textwidth}{
            \begin{itemize}
	            \item Red/Ox : Oxygen vacancies \rlap{migration}
	            \item Low voltage operation
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Initial forming phase required : CMOS process incompatibility
            \end{itemize} }} \\
	        \hline
	        \textbf{CBRAM} \newline Conductive~Bridge RAM &  \vspace{-.022\textheight}\includegraphics[width=.4\textwidth]{figures/CBRAM_Valov2013.png} & {\parbox{0.37\textwidth}{
            \begin{itemize}
				      \item Red/Ox : metallic filament formation
				      \item No forming phase but higher voltage in operation
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Limited endurance
            \end{itemize} }} \\
	        \hline
	        \vspace{-.018\textheight} \textbf{STT-MRAM} \newline Spin~Transfer \newline Torque Magneto-resistive RAM & \vspace{-.018\textheight}\includegraphics[width=.4\textwidth]{figures/Layer2016-pstt.png} & {\parbox{0.37\textwidth}{
            \begin{itemize}
	            \item Magnetic Tunnel Junction
	            \item Trade-off : write energy vs retention
              \item[{\small \textcolor{red}{$\blacktriangleright$}}]High write energy
            \end{itemize} }} \\
          \hline
	      \end{tabular}
	    \end{table}
      \small
      Illustrations:
      \begin{columns}[T]
        \begin{column}{.5\textwidth}
          FRAM \fullcitebib{zhang2015organic}
          PCM \fullcitebib{hegedus2008microscopic}
          ReRAM \fullcitebib{wei2013}
        \end{column}
        \begin{column}{.5\textwidth}
          CBRAM \fullcitebib{valov2013cation}
          STT-MRAM \fullcitebib{Layer2016}
        \end{column}
      \end{columns}
      \normalsize
		\end{tcolorbox}
	\end{column}
%------------------------------------------------------------------------------%
	\begin{column}{.493\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main,title={\begin{center}\Large{\textbf{Normally-Off Computing}}\end{center}}]
      \vspace{-.01\textheight}
			\begin{columns}[T]
				\begin{column}{.32\textwidth}
          \paragraphtitle{Programming model}
				  \begin{tcolorbox}[boxsep=1pt,left=10pt,right=10pt,top=5pt,bottom=5pt, colback=white]
			  	  \begin{minted}{c}
main()
{
 inputs i
 outputs o
 internal_state s
 time_to_next_boot t
 while(true) {
  i = get_inputs()
  {o,t,s} = compute(i,s)
  write_outputs(o)
  shut_down(t)
 }
}
  				  \end{minted}
          \end{tcolorbox}
  			\end{column}
  			\begin{column}{.655\textwidth}
          \paragraphtitle{Concepts}
  				\begin{itemize}
  					\item $Idle\_Ratio = 1 - {\displaystyle \frac{time\_in\_active\_mode}{total\_time}}$
            \begin{itemize}
              \item Workload dependent
            \end{itemize}
  					\item Power on/off overhead : energy spent in on/off transitions
            \begin{itemize}
              \item Platform dependent
            \end{itemize}
  				\end{itemize}
          \vspace{-.008\textheight}
          \noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}\\
          \paragraphtitle{Interaction with NVRAM}
          \begin{itemize}
            \item Intermediate computation results reuse between life cycles
            \item Applications don't need to store and fetch from storage memory
          \end{itemize}
          \vspace{-.008\textheight}
          \noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}\\
          \paragraphtitle{Application Domain}
          Audio and video processing autonomous devices, biometrics, security and health monitoring
        \end{column}
      \end{columns}
      \vspace{.005\textheight}
      \begin{tcolorbox}[boxsep=1pt,left=10pt,right=10pt,top=5pt,bottom=5pt, colback=white]
        \includegraphics[width=\textwidth]{figures/Layer2016-NOff.png}
        \begin{center}\parencite{Layer2016}\end{center}
      \end{tcolorbox}
      \begin{center}
        \textcolor{red}{In red} : Energy consumed on Always-On SRAM based SoC\\
        \textcolor{green}{In green} : Energy consumed on Normally-Off STT-MRAM SoC
      \end{center}
      \paragraphtitle{Higher cost of NVRAM usage can be offset by intermittent execution}
      \vspace{-.01\textheight}
		\end{tcolorbox}
	\end{column}
\end{columns}
\vspace{.01\textheight}
%------------------------------------------------------------------------------%
% problematique
%------------------------------------------------------------------------------%
\vspace{-.005\textheight}
\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main,title={\begin{center}\Large{\textbf{Problem Statement: How to take advantage of the NVRAM properties and overcome their limitations?}}\end{center}}]
  \begin{columns}[T]
    \begin{column}{.3\textwidth}
    \end{column}
    \begin{column}{.7\textwidth}
      \begin{itemize}
        \item {\large How to hide memory management from the application developer ?}
      \end{itemize}
    \end{column}
  \end{columns}
  \begin{columns}[T]
    \begin{column}{.05\textwidth}
    \end{column}
    \begin{column}{.5\textwidth}
      \begin{itemize}
        \item {\large How to place (and relocate) code, heap and stack on heterogeneous memory banks ?}
      \end{itemize}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{itemize}
        \item {\large How to support dynamic memory allocation in these conditions ?}
      \end{itemize}
    \end{column}
  \end{columns}
\end{tcolorbox}
%------------------------------------------------------------------------------%
% archi et simu | memory management
%------------------------------------------------------------------------------%
\vspace{-.002\textheight}
\begin{columns}[T]
	\begin{column}{.74\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=6pt,colframe=main,title={\begin{center}\Large{\textbf{Architecture Exploration and SoC Simulation}}\end{center}}]
      %\vspace{-.01\textheight}
			\begin{columns}[T]
				\begin{column}{.48\textwidth}
          % \paragraphtitle{Example Target Architecture}
          \begin{tcolorbox}[boxsep=1pt,left=10pt,right=10pt,top=5pt,bottom=5pt, colback=white]
            \includegraphics[width=.98\textwidth]{figures/Layer2016-SoC.png}
            \begin{center}\parencite{Layer2016}\end{center}
          \end{tcolorbox}
  			\end{column}
  			\begin{column}{.50\textwidth}
          \paragraphtitle{Scratch Pad Memory model}
          \begin{tcolorbox}[boxsep=1pt,left=10pt,right=10pt,top=5pt,bottom=5pt, colback=white]
            \includegraphics[width=.9\textwidth]{figures/spm_evaderis.pdf}
          \end{tcolorbox}
          \vspace{.005\textheight}
          \paragraphtitle{Architecture Exploration}
          \begin{itemize}
            \item Size, density, speed, NVRAM technology
            \item Specific DMA design for NVRAM management
          \end{itemize}
          \noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}\\
          \vspace{.005\textheight}
          \paragraphtitle{Simulation}
          \begin{itemize}
            \item Platform simulated in SystemC TLM
            \item Using a cycle accurate ISS
            \item Offline energy simulation
          \end{itemize}
  			\end{column}
  		\end{columns}
		\end{tcolorbox}
	\end{column}
%------------------------------------------------------------------------------%
	\begin{column}{.2593\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main,title={\begin{center}\Large{\textbf{Road Map}}\end{center}}]
    \paragraphtitle{Understand Memory Access Profiles}
    \begin{itemize}
      \item Run benchmarks on our simulated platform: signal processing, cryptographic algorithm, ...
      \item Build profiles with collected traces
      \item Evaluate using a realistic application
      \item Normally-Off execution
    \end{itemize}
    \noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}\\
    \vspace{-.005\textheight}
    \paragraphtitle{Static Placement Strategies}
    \begin{itemize}
      \item Segregate hot and cold data in appropriate memories
      \item Assess whether add RAM would be profitable
    \end{itemize}
    \noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}\\
    \vspace{-.005\textheight}
		\paragraphtitle{Dynamic Memory management}
    \begin{itemize}
      \item Explore the energy vs retention trade-off
      \item Extend the endurance of NVRAM cells via wear-leveling
      \item HW/ SW codesign to extend memory management capabilities
    \end{itemize}
    \vspace{.01\textheight}
    \begin{center}
      \small{Sponsored by R\'{e}gion Rh\^{o}ne Alpes \mbox{ADR 16-005688-01.}}
    \end{center}
    \vspace{.0055\textheight}
    % \vspace{-.0075\textheight}
		\end{tcolorbox}
	\end{column}
\end{columns}
%------------------------------------------------------------------------------%
% logos
%------------------------------------------------------------------------------%
% géré dans le thème :
% \includegraphics[width=\paperwidth]{figures/logos.png}
%==============================================================================%
\end{frame}
%==============================================================================%
%==============================================================================%
\end{document}
%==============================================================================%


