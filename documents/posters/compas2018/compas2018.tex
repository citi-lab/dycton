\documentclass[final,t]{beamer}
\mode<presentation>{\usetheme{RWSposter}}
\usepackage{ragged2e}

%==============================================================================%
% additional settings
% \setbeamerfont{itemize}{size=\normalsize}
% \setbeamerfont{itemize/enumerate body}{size=\normalsize}
% \setbeamerfont{itemize/enumerate subbody}{size=\normalsize}

%==============================================================================%
% additional packages
\usepackage[orientation=portrait,size=a0b,scale=1.1]{beamerposter}
\usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage{exscale}
\usepackage{booktabs, array}
\usepackage{algorithm, algorithmic}
\usepackage{xspace}
\usepackage{wrapfig}
\usepackage{multicol}
\usepackage{tcolorbox}
\usepackage{minted}
\usepackage{graphicx}
\usepackage{array}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}

\usemintedstyle{tango}

%==============================================================================%
\listfiles
\title{Towards Dynamic Memory Management for Heterogenous Memory in Embedded Systems}
\author{Tristan Delizy - \normalsize{Ph.D. Student}}
\advisors{Advisors : Guillaume Salagnac, Kevin Marquet, Matthieu Moy, Tanguy Risset}
\partnership{Industrial Partner : eVaderis}
\addressints{}
\institutes{Univ Lyon, Inria, INSA Lyon, CITI Laboratory}
\event{Compas 2018}
\email{tristan.delizy@insa-lyon.fr}


%%%%%%%%%%%%%%%%%%%%
% Bibliographic references
\usepackage[
backend=bibtex,
maxnames=2,
minnames=2,
maxbibnames=10,
bibencoding=inputenc,
bibstyle=authoryear,
style=numeric,
citestyle=numeric,
% citestyle=authoryear-comp,
sorting=anyt, % Sort by alphabetic label, name, year, title
doi=false,
isbn=false,
url=false,
uniquename=init,
]{biblatex}
\addbibresource{ref.bib}
% or: plain,unsrt,alpha,abbrv,acm,apalike,...

% \AtBeginBibliography{\small}

% for in text full cite
% \DeclareCiteCommand{\fullcitebib}
%   {\renewcommand{\finalnamedelim}
%    {\ifnum\value{liststop}>2 \finalandcomma\fi\addspace\&\space}%
%    \list{}
%    {\setlength{\leftmargin}{\bibhang}%
%      \setlength{\itemindent}{-\leftmargin}%
%      \setlength{\itemsep}{\bibitemsep}%
%      \setlength{\parsep}{\bibparsep}}\item}
%   {\usedriver
%     {}
%     {\thefield{entrytype}}\finentry}
%   {\item}
%   {\endlist\global\undef\bbx}
% \makeatother

\renewcommand*{\bibfont}{\small}
\setbeamertemplate{bibliography item}{\insertbiblabel}

\makeatletter

\newrobustcmd*{\parentexttrack}[1]{%
  \begingroup
  \blx@blxinit
  \blx@setsfcodes
  \blx@bibopenparen#1\blx@bibcloseparen
  \endgroup}

\AtEveryCite{%
  \let\parentext=\parentexttrack%
  \let\bibopenparen=\bibopenbracket%
  \let\bibcloseparen=\bibclosebracket}

\makeatother

\definecolor{main}{RGB}{87, 195, 96}
\definecolor{main_text}{RGB}{55, 124, 61}
\definecolor{main_back}{RGB}{242, 250, 243}

\setbeamercolor{bibliography entry author}{fg=main_text}
\setbeamercolor{bibliography entry note}{fg=black}
\setbeamercolor{bibliography item}{fg=black}
\setbeamercolor{itemize item}{fg=main_text}



\newcommand{\paragraphtitle}[1]{\begin{center}\textbf{#1}\end{center}}


%==============================================================================%
\begin{document}
%==============================================================================%
%==============================================================================%
\begin{frame}[fragile]
%==============================================================================%
% titre (géré dans le thème)
%------------------------------------------------------------------------------%

%------------------------------------------------------------------------------%
% emmerging NV mems | implications for embedded devices
%------------------------------------------------------------------------------%
\vspace{-.005\textheight}
\begin{columns}[T]
	\begin{column}{.493\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main, colback=main_back,title={\begin{center}\Large{\textbf{Emerging Non-Volatile Memories}}\end{center}}]
	    \begin{table}
	    \vspace{-.012\textheight}
	       \begin{tabular}{ p{.2\textwidth}  P{.42\textwidth}  p{.37\textwidth} }
	        \textbf{FRAM} \newline Ferroelectric~RAM & \vspace{-.024\textheight}\hspace{.05\textwidth}\includegraphics[height=.06\textheight]{Figures/Fram_hysteresis.png} & {\parbox{0.37\textwidth}{
            \vspace{.012\textheight}
            \begin{itemize}
              \item Charge trap
              \item Read operation is destructive
              \item 125nm limitation
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Low capacity
            \end{itemize} }} \\
	        \hline
	        \textbf{PCM} \newline Phase~Change \newline Memory & \vspace{-.018\textheight}\includegraphics[height=.06\textheight]{Figures/pcm_cristal.png} & {\parbox{0.37\textwidth}{
            \vspace{.015\textheight}
            \begin{itemize}
              \item Amorphous / crystalline state
              \item Variability in operation \rlap{currents}
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Limited write endurance
            \end{itemize} }} \\
	        \hline
	        \textbf{ReRAM} \newline Resistive~RAM &  \vspace{-.024\textheight}\includegraphics[height=.06\textheight]{Figures/RERAM_Wei2013.png} & {\parbox{0.37\textwidth}{
            \vspace{.005\textheight}
            \begin{itemize}
              \item[]
	          \item Red/Ox: Oxygen vacancies \rlap{migration}
	          \item Low voltage operation
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] CMOS process incompatibility
            \end{itemize} }} \\
	        \hline
	        \textbf{CBRAM} \newline Conductive~Bridge\newline RAM &  \vspace{-.022\textheight}\includegraphics[height=.055\textheight]{Figures/CBRAM_Valov2013.png} & {\parbox{0.37\textwidth}{
            \vspace{.002\textheight}
            \begin{itemize}
              \item[]
			  \item Red/Ox: metallic filament formation
			  \item high voltage in operation
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Limited endurance
            \end{itemize} }} \\
	        \hline
	        \vspace{-.018\textheight} \textbf{STT-MRAM} \newline Spin~Transfer Torque \newline Magnetoresistive~RAM & \vspace{-.018\textheight}\includegraphics[height=.048\textheight]{Figures/Layer2016-pstt.png} & {\parbox{0.37\textwidth}{
            \vspace{.005\textheight}
            \begin{itemize}
              \item[]
	          \item Magnetic Tunnel Junction
	          \item Trade-off : write energy vs retention
              \item[{\small \textcolor{red}{$\blacktriangleright$}}]R/W latency assymetry
            \end{itemize} }}
    	      \end{tabular}
	       \end{table}
            \vspace{-.01\textheight}
		\end{tcolorbox}
	\end{column}
%------------------------------------------------------------------------------%
	\begin{column}{.493\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main, colback=main_back, title={\begin{center}\Large{\textbf{Trends in Embedded System Design}}\end{center}}]
            \vspace{-.01\textheight}
			\begin{columns}[T]
				\begin{column}{.49\textwidth}
                \paragraphtitle{Heterogeneous Memory Architectures}
            	\vspace{-.005\textheight}
                \begin{itemize}
                    \item[] Scratch Pad Memories:
	                \item Energy efficient
	                \item No caches and no MMU
	                \item Must be managed by software
                \end{itemize}
  			\end{column}
  			\begin{column}{.51\textwidth}
          		\paragraphtitle{Normally-off Computing}
            	\vspace{-.005\textheight}
          		\begin{itemize}
            		\item Power off instead of low power modes
            		\item NVRAM: consumes more when active, no \rlap{leakage}
            		\item allow to leverage higher energy cost of NVRAM
          		\end{itemize}
        	\end{column}
      	\end{columns}
        % \vspace{-.01\textheight}
          \begin{tcolorbox}[boxsep=1pt,left=10pt,right=10pt,top=5pt,bottom=5pt, colback=white]
            \begin{center}
                \includegraphics[width=.7\textwidth]{Figures/Layer2016-SoC.png}\\
        		\vspace{-.005\textheight}
                \paragraphtitle{Our Reference Architecture : Embedded SoC with Heterogeneous Memory Architecture}
            \end{center}
          \end{tcolorbox}
        \vspace{-.01\textheight}
		\end{tcolorbox}
	\end{column}
\end{columns}
%------------------------------------------------------------------------------%
% problematique
%------------------------------------------------------------------------------%
	    \vspace{.005\textheight}
\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main, colback=main_back,title={\begin{center}\Large{\textbf{Problem Statement: How to take advantage of the NVRAM properties and overcome their limitations?}}\end{center}}]
  \begin{columns}[T]
    \begin{column}{.5\textwidth}
      \begin{itemize}
        \item {\large How to hide Memory Heterogeneity from the application developer ?}
      \end{itemize}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{itemize}
        \item {\large How to support efficient dynamic memory allocation in these conditions ?}
      \end{itemize}
    \end{column}
  \end{columns}
\end{tcolorbox}
%------------------------------------------------------------------------------%
% SPM management | dynamic memory management
%------------------------------------------------------------------------------%
	    \vspace{.005\textheight}
\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=6pt,colframe=main, colback=main_back,title={\begin{center}\Large{\textbf{Dynamic Memory Allocation in Heterogenous Memory}}\end{center}}]

\begin{columns}[T]
      \begin{column}{.4\textwidth}
	    \vspace{.03\textheight}
		  \begin{tcolorbox}[boxsep=0pt,left=10pt,right=10pt,top=5pt,bottom=5pt, colback=white]
            \includegraphics[width=\textwidth]{Figures/sw_hw.pdf}
  		  \begin{center}
  		    \paragraphtitle{Software Layer for Allocation in Multiple Memory Banks }
  		  \end{center}
          \end{tcolorbox}
  		  % \vspace{.01\textheight}
      \end{column}
      \begin{column}{.3\textwidth}
		\begin{center}
          \paragraphtitle{Software Architecture}
        \end{center}
  		\vspace{.007\textheight}
        \begin{itemize}
            \item Keep usual \texttt{malloc() / free()} API
            \item \emph{Placement problem}: in which bank to allocate each object?
            \item Our approach: one \emph{allocator} per bank and one \emph{dispatcher}
            \item This study: estimate achievable gains
        \end{itemize}
  		\vspace{.01\textheight}
		\begin{center}
          \paragraphtitle{Dispatching Strategies}
        \end{center}
  		\vspace{.007\textheight}
        \begin{itemize}
		  \item Baseline: Fast First, fallback to slow heap if full
		  \item[]
		  \item This study: near-optimal clairvoyant strategies
		  \begin{itemize}
   		    \item Integer Linear Programming
  		    \item Theoretical Upper Bound
  		  \end{itemize}
		  \item[]
		  \item Proposed metric:
		  \begin{itemize}
  		    \item Access density to each heap object
  		    \item Formula: $density =$ {\large$\frac{N_{reads} + N_{writes}}{size \times lifetime}$}
  		  \end{itemize}
        \end{itemize}
      \end{column}
      \begin{column}{.3\textwidth}
		\begin{center}
          \paragraphtitle{Experimental setup}
        \end{center}
  		\vspace{.007\textheight}
  		\begin{tabular}{l c c c}
  		  \hline
  		  Application		&~~~Object Count~~~& Maximum Heap Size \\
  		  \hline
  		  \textbf{json}     & 1638      	& 38 kB         \\
  		  \textbf{dijkstra} & 14980     & 10 kB         \\
  		  \textbf{jpg2000}	& 10257     	& 1665 kB 	\\
  		  \textbf{h263}     & 53821     	& 1232 kB       \\
  		  \hline
  		\end{tabular}\\
  		\vspace{.01\textheight}
        % \end{center}
		\begin{itemize}
		  \item Code, Stack, Globals = ideal memory (no latency)
		  \item Experiment 1:  heap in SRAM and generic NVRAM
		  \begin{itemize}
  			\item SRAM: no latency
  			\item generic NVRAM: symmetric 10 cycle latency
          \end{itemize}
          \item[]
		  \item Experiment 2: heap in fast MRAM and dense MRAM
		  \begin{itemize}
  			\item Fast MRAM: read=1 cycle, write=3 cycles
 			\item Dense MRAM: read=2 cycles, write=30 cycles
          \end{itemize}
        \end{itemize}
		% \begin{center}
  		\vspace{.01\textheight}
		\begin{tabular}{ l c c c c c c c}
		  \hline
		   Arch.	& A0 & A1 & A2 & A3 & A4 & A5 & A6  \\
		  \hline
		   Fast 	& 100\% & 75\%  & 50\%  & 25\%  & 10\%  & 5\%  & 0\% \\
		   Slow 	& 0\%  & 25\% & 50\% & 75\% & 90\% & 95\% & 100\% \\
		  \hline
		\end{tabular}
		% \begin{tabular}{ c c c}
		%   \hline
		%    Scenarios					& Fast  & Slow  \\
		%    (heaps memory technologies)   & R/W 	& R/W   \\
		%   \hline
		%    (1) SRAM + NVRAM 		  	& 1/1 	& 10/10 \\
		%    (2) fast MRAM + dense MRAM 	& 1/3	& 2/30 	\\
		%   \hline
		% \end{tabular}
        % \end{center}
      \end{column}
\end{columns}
  		\vspace{.005\textheight}

\begin{tcolorbox}[boxsep=1pt,left=20pt,right=15pt,top=5pt,bottom=5pt, colback=white]
\begin{columns}[T]
      \begin{column}{.009\textwidth}
      	\vspace{.04\textheight}
      	\rotatebox{90}{{\large Experiment 1}}\\
      	\vspace{.06\textheight}
      	\rotatebox{90}{{\large Experiment 2}}
      \end{column}
      \begin{column}{.245\textwidth}
        \begin{center}
          \includegraphics[trim=25 28 10 10,clip,width=\textwidth]{Figures/json_speedup_XP1.pdf}\\
          \includegraphics[trim=25 28 10 28,clip,width=\textwidth]{Figures/json_speedup_XP2.pdf}\\
        \end{center}
      \end{column}
     \begin{column}{.245\textwidth}
        \begin{center}
          \includegraphics[trim=25 28 10 10,clip,width=\textwidth]{Figures/dijkstra_speedup_XP1.pdf}\\
          \includegraphics[trim=25 28 10 28,clip,width=\textwidth]{Figures/dijkstra_speedup_XP2.pdf}\\
        \end{center}
      \end{column}
      \begin{column}{.245\textwidth}
        \begin{center}
          \includegraphics[trim=25 28 10 10,clip,width=\textwidth]{Figures/jpg2000_speedup_XP1.pdf}\\
          \includegraphics[trim=25 28 10 28,clip,width=\textwidth]{Figures/jpg2000_speedup_XP2.pdf}\\
        \end{center}
      \end{column}
     \begin{column}{.245\textwidth}
        \begin{center}
          \includegraphics[trim=25 28 10 10,clip,width=\textwidth]{Figures/h263_speedup_XP1.pdf}\\
          \includegraphics[trim=25 28 10 28,clip,width=\textwidth]{Figures/h263_speedup_XP2.pdf}\\
        \end{center}
      \end{column}
    \end{columns}
    {\centering \paragraphtitle{Results: Application speedup (\%) in function of heap fraction in fast memory}}
\end{tcolorbox}
%------------------------------------------------------------------------------%
\end{tcolorbox}
% logos
%------------------------------------------------------------------------------%
% géré dans le thème :
% \includegraphics[width=\paperwidth]{Figures/logos.png}
%==============================================================================%
\end{frame}
%==============================================================================%
%==============================================================================%
\end{document}
%==============================================================================%


