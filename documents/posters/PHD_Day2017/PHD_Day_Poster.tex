\documentclass[final,t]{beamer}
\mode<presentation>{\usetheme{RWSposter}}
\usepackage{ragged2e}

%==============================================================================%
% additional settings
\setbeamerfont{itemize}{size=\normalsize}
\setbeamerfont{itemize/enumerate body}{size=\normalsize}
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}

%==============================================================================%
% additional packages
\usepackage[orientation=portrait,size=a0b,scale=1.1]{beamerposter}
\usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage{exscale}
\usepackage{booktabs, array}
\usepackage{algorithm, algorithmic}
\usepackage{xspace}
\usepackage{wrapfig}
\usepackage{multicol}
\usepackage{tcolorbox}
\usepackage{minted}

\usepackage{array}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}

\usemintedstyle{tango}

%==============================================================================%
\listfiles
\title{Towards Dynamic Scratch Pad Memory Management for Embedded Systems}
\author{Tristan Delizy - \normalsize{ 1$^{st}$ year Ph.D. Student}}
\advisors{Advisors : Guillaume Salagnac, Kevin Marquet, Matthieu Moy, Tanguy Risset}
\partnership{Industrial Partner : eVaderis}
\addressints{}
\institutes{Univ Lyon, Inria, INSA Lyon, CITI Laboratory}
\event{CITI PHD Day 2017}
\email{tristan.delizy@insa-lyon.fr}


%%%%%%%%%%%%%%%%%%%%
% Bibliographic references
\usepackage[
backend=bibtex,
maxnames=2,
minnames=2,
maxbibnames=10,
bibencoding=inputenc,
bibstyle=authoryear,
style=numeric,
citestyle=numeric,
% citestyle=authoryear-comp,
sorting=anyt, % Sort by alphabetic label, name, year, title
doi=false,
isbn=false,
url=false,
uniquename=init,
]{biblatex}
\addbibresource{ref.bib}
% or: plain,unsrt,alpha,abbrv,acm,apalike,...

% \AtBeginBibliography{\small}

% for in text full cite
% \DeclareCiteCommand{\fullcitebib}
%   {\renewcommand{\finalnamedelim}
%    {\ifnum\value{liststop}>2 \finalandcomma\fi\addspace\&\space}%
%    \list{}
%    {\setlength{\leftmargin}{\bibhang}%
%      \setlength{\itemindent}{-\leftmargin}%
%      \setlength{\itemsep}{\bibitemsep}%
%      \setlength{\parsep}{\bibparsep}}\item}
%   {\usedriver
%     {}
%     {\thefield{entrytype}}\finentry}
%   {\item}
%   {\endlist\global\undef\bbx}
% \makeatother

\renewcommand*{\bibfont}{\small}
\setbeamertemplate{bibliography item}{\insertbiblabel}

\makeatletter

\newrobustcmd*{\parentexttrack}[1]{%
  \begingroup
  \blx@blxinit
  \blx@setsfcodes
  \blx@bibopenparen#1\blx@bibcloseparen
  \endgroup}

\AtEveryCite{%
  \let\parentext=\parentexttrack%
  \let\bibopenparen=\bibopenbracket%
  \let\bibcloseparen=\bibclosebracket}

\makeatother

\definecolor{main}{RGB}{87, 195, 96}
\definecolor{main_text}{RGB}{55, 124, 61}
\definecolor{main_back}{RGB}{242, 250, 243}

\setbeamercolor{bibliography entry author}{fg=main_text}
\setbeamercolor{bibliography entry note}{fg=black}
\setbeamercolor{bibliography item}{fg=black}
\setbeamercolor{itemize item}{fg=main_text}



\newcommand{\paragraphtitle}[1]{\begin{center}\textbf{#1}\end{center}}


%==============================================================================%
\begin{document}
%==============================================================================%
%==============================================================================%
\begin{frame}[fragile]
%==============================================================================%
% titre (géré dans le thème)
%------------------------------------------------------------------------------%

%------------------------------------------------------------------------------%
% emmerging NV mems | implications for embedded devices
%------------------------------------------------------------------------------%
\vspace{-.005\textheight}
\begin{columns}[T]
	\begin{column}{.493\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main, colback=main_back,title={\begin{center}\Large{\textbf{Emerging Non-Volatile Memories}}\end{center}}]
	    \begin{table}
	    \vspace{-.012\textheight}
	       \begin{tabular}{ p{.2\textwidth}  P{.42\textwidth}  p{.37\textwidth} }
	        \textbf{FRAM} \parencite{zhang2015organic} \newline Ferroelectric~RAM & \vspace{-.024\textheight}\hspace{.05\textwidth}\includegraphics[height=.06\textheight]{Figures/Fram_hysteresis.png} & {\parbox{0.37\textwidth}{
            \vspace{.012\textheight}
            \begin{itemize}
              \item Charge trap
              \item Read operation is destructive
              \item 125nm limitation
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Low capacity
            \end{itemize} }} \\
	        \hline
	        \textbf{PCM} \parencite{hegedus2008microscopic} \newline Phase~Change \newline Memory & \vspace{-.018\textheight}\includegraphics[height=.06\textheight]{Figures/pcm_cristal.png} & {\parbox{0.37\textwidth}{
            \vspace{.015\textheight}
            \begin{itemize}
              \item Amorphous / crystalline state
              \item Variability in operation \rlap{currents}
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Limited write endurance
            \end{itemize} }} \\
	        \hline
	        \textbf{ReRAM} \parencite{wei2013} \newline Resistive~RAM &  \vspace{-.024\textheight}\includegraphics[height=.06\textheight]{Figures/RERAM_Wei2013.png} & {\parbox{0.37\textwidth}{
            \vspace{.005\textheight}
            \begin{itemize}
              \item[]
	          \item Red/Ox: Oxygen vacancies \rlap{migration}
	          \item Low voltage operation
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] CMOS process incompatibility
            \end{itemize} }} \\
	        \hline
	        \textbf{CBRAM} \parencite{valov2013cation} \newline Conductive~Bridge\newline RAM &  \vspace{-.022\textheight}\includegraphics[height=.055\textheight]{Figures/CBRAM_Valov2013.png} & {\parbox{0.37\textwidth}{
            \vspace{.002\textheight}
            \begin{itemize}
              \item[]
			  \item Red/Ox: metallic filament formation
			  \item high voltage in operation
              \item[{\small \textcolor{red}{$\blacktriangleright$}}] Limited endurance
            \end{itemize} }} \\
	        \hline
	        \vspace{-.018\textheight} \textbf{STT-MRAM} \parencite{Layer2016} \newline Spin~Transfer Torque \newline Magnetoresistive~RAM & \vspace{-.018\textheight}\includegraphics[height=.048\textheight]{Figures/Layer2016-pstt.png} & {\parbox{0.37\textwidth}{
            \vspace{.005\textheight}
            \begin{itemize}
              \item[]
	          \item Magnetic Tunnel Junction
	          \item Trade-off : write energy vs retention
              \item[{\small \textcolor{red}{$\blacktriangleright$}}]R/W latency assymetry
            \end{itemize} }} \\
              \hline
    	      \end{tabular}
	       \end{table}
            References:
            \printbibliography
            \vspace{.0008\textheight}
		\end{tcolorbox}
	\end{column}
%------------------------------------------------------------------------------%
	\begin{column}{.493\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main, colback=main_back, title={\begin{center}\Large{\textbf{Implications for Embedded Devices}}\end{center}}]
			\begin{columns}[T]
				\begin{column}{.5\textwidth}
                \paragraphtitle{Scratch Pad Memories}
                \begin{itemize}
                    \item Known to be more energy efficient than caches for low power embedded devices \parencite{Banakar2002}
                    \item More predictable for real time applications
                    \item Denser memory technologies leads to increased relative memory size
                    \item Must be managed by software
                \end{itemize}
  			\end{column}
  			\begin{column}{.5\textwidth}
          \paragraphtitle{normally off}
          \begin{itemize}
            \item Memory Subsystem is an important source of SoC energy consumption
            \item Shut down instead of low power modes with checkpointing strategy
            \item NVRAM consumes more in active mode but have no leakage currents when not active
          \end{itemize}
        \end{column}
      \end{columns}
      \vspace{.01\textheight}
        \begin{columns}[T]
            \begin{column}{.5\textwidth}
                \begin{center}
                    $\rightarrow$ Good architecture candidate for inSoC NVRAM integration
                \end{center}
            \end{column}
            \begin{column}{.5\textwidth}
          \begin{center}
              $\rightarrow$ allow to leverage higher energy cost of NVRAM \parencite{Layer2016}
          \end{center}
            \end{column}
        \end{columns}
      \vspace{.01\textheight}
          \begin{tcolorbox}[boxsep=1pt,left=10pt,right=10pt,top=5pt,bottom=5pt, colback=white]
            \begin{center}
                \includegraphics[width=.8\textwidth]{Figures/Layer2016-SoC.png}\\
                \paragraphtitle{Example Architecture : Embedded SoC with Heterogeneous Memory Hierarchy \parencite{Layer2016}}
            \end{center}
          \end{tcolorbox}
      \vspace{.01\textheight}
        \paragraphtitle{Application Domains}
        \vspace{.005\textheight}
        \begin{center}
            This approach leads to architectures with inSoC heterogeneous memory subsystem that fit the following domains requirements:
        \end{center}

        \begin{columns}[T]
            \begin{column}{.08\textwidth}
            \end{column}
            \begin{column}{.37\textwidth}
                \begin{itemize}
                \item Audio and video processing
                \item autonomous devices
                \end{itemize}
            \end{column}
            \begin{column}{.30\textwidth}
                \begin{itemize}
                \item security
                \item health monitoring
                \end{itemize}
            \end{column}
            \begin{column}{.25\textwidth}
                \begin{itemize}
                \item biometrics
                \end{itemize}
            \end{column}
        \end{columns}
      \vspace{.005\textheight}
		\end{tcolorbox}
	\end{column}
\end{columns}
\vspace{.01\textheight}
%------------------------------------------------------------------------------%
% problematique
%------------------------------------------------------------------------------%
\vspace{-.005\textheight}
\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main, colback=main_back,title={\begin{center}\Large{\textbf{Problem Statement: How to take advantage of the NVRAM properties and overcome their limitations?}}\end{center}}]
  \begin{columns}[T]
    \begin{column}{.5\textwidth}
      \begin{itemize}
        \item {\large How to hide Scratch Pad Memory management from the application developer ?}
      \end{itemize}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{itemize}
        \item {\large How to support efficient dynamic memory allocation in these conditions ?}
      \end{itemize}
    \end{column}
  \end{columns}
\end{tcolorbox}
%------------------------------------------------------------------------------%
% SPM management | dynamic memory management
%------------------------------------------------------------------------------%
\vspace{-.002\textheight}
\begin{columns}[T]
	\begin{column}{.31\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=6pt,colframe=main, colback=main_back,title={\begin{center}\Large{\textbf{Scratch Pad Memory Managment}}\end{center}}]
          \paragraphtitle{Compile Time Allocation}
          \textbf{Static}
            \begin{itemize}
                \item Compute a optimal layout for most frequent accessed memory locations \parencite{avissar2002optimal}
                \item Variables retention time in different banks to exploit the energy vs retention trade-off \parencite{Rodriguez2014}
            \end{itemize}
          \textbf{Dynamic}
            \begin{itemize}
                \item Data Layout Decision Problem: compute a SPM layout with load and unload instructions inserted by compiler \parencite{Cho2009}
                \item Code and stack managment techniques: working on intensive loops and procedures
            \end{itemize}
            \vspace{-.005\textheight}
          \noindent\makebox[\linewidth]{\rule{.5\textwidth}{0.4pt}}\\
          \vspace{-.01\textheight}
          \begin{center}
            \begin{itemize}
                \item These techniques focus on layout mapping for small size SPMs
                \item Not efficient If significant part of memory used is allocated at runtime, these strategy could be suboptimals
            \end{itemize}
          \end{center}
          \paragraphtitle{Runtime Allocation}
          \begin{itemize}
            \item Based on Programmer's annotations, runtime allocation shows good performance improvments for image processing, security and graphs algorithms \parencite{Muck2011}
          \end{itemize}
		\end{tcolorbox}
	\end{column}
%------------------------------------------------------------------------------%
	\begin{column}{.69\textwidth}
		\begin{tcolorbox}[boxsep=20pt,left=10pt,right=10pt,top=10pt,bottom=10pt,colframe=main, colback=main_back,title={\begin{center}\Large{\textbf{Towards NVRAM Runtime Allocation}}\end{center}}]
            \begin{columns}[T]
                \begin{column}{.69\textwidth}
          % \paragraphtitle{Example Target Architecture}
          \vspace{.005\textheight}
          \begin{tcolorbox}[boxsep=1pt,left=10pt,right=10pt,top=5pt,bottom=5pt, colback=white]
          \vspace{.005\textheight}
            \begin{center}
              \includegraphics[height=.22\textheight]{Figures/access_main_mem.pdf}
              \includegraphics[height=.22\textheight]{Figures/RW_main_mem.pdf}
            \end{center}
          \end{tcolorbox}
            \end{column}
            \begin{column}{.31\textwidth}
    \paragraphtitle{Understand and Exploit Memory Access Profiles}
    \begin{itemize}
      \item Run benchmarks on our simulated platform: signal processing, cryptographic algorithm\rlap{...}
      \item Build profiles with collected traces
      \item Evaluate using a realistic application
      \item Normally-Off execution
    \end{itemize}
    \noindent\makebox[\linewidth]{\rule{.5\textwidth}{0.4pt}}\\
    \vspace{.01\textheight}
		\paragraphtitle{Dynamic Memory management}
    \begin{itemize}
      \item Propose a Memory Manager for inSoC heterogeneous memory
      \item HW/ SW codesign to extend memory management capabilities
      \item Extend the endurance of NVRAM cells via wear-leveling
    \end{itemize}
    \vspace{.01\textheight}
    \begin{center}
      \small{Sponsored by R\'{e}gion Rh\^{o}ne Alpes \mbox{ADR 16-005688-01.}}
    \end{center}
            \end{column}
            \end{columns}
            \vspace{.005\textheight}
		\end{tcolorbox}
	\end{column}
\end{columns}
%------------------------------------------------------------------------------%
% logos
%------------------------------------------------------------------------------%
% géré dans le thème :
% \includegraphics[width=\paperwidth]{Figures/logos.png}
%==============================================================================%
\end{frame}
%==============================================================================%
%==============================================================================%
\end{document}
%==============================================================================%


