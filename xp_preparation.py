#!/usr/bin/env python3

from src.scripts.obj_analysis import analyse_object
from src.scripts.dycton_gen_exec import generate_executable
from src.scripts.dycton_gen_placement import generate_placement
from src.scripts.header_profile_gen import generate_header_profile
from datetime import datetime
from os import mkdir, path, chmod, stat
from contextlib import redirect_stdout
from shutil import copyfile, copytree, move
import sys
import subprocess
import stat
import argparse
import re

# get xp configuration
import xp_config as xp

def experiment_preparation():
  # start by printing the date
  date = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

  # Get memory latencies from address_map.h
  amap_lines = []
  m_fast_rlat, m_fast_wlat, m_slow_rlat, m_slow_wlat = 0, 0, 0, 0
  with open("src/platform_tlm/address_map.h") as address_map:
    for line in address_map:
      if "//" not in line and "#define MEM_" in line:
        amap_lines.append(line)
        if "MEM_FAST_RLAT" in line:
          m_fast_rlat = int(line.split("(")[1].split(")")[0])
        if "MEM_FAST_WLAT" in line:
          m_fast_wlat = int(line.split("(")[1].split(")")[0])
        if "MEM_SLOW_RLAT" in line:
          m_slow_rlat = int(line.split("(")[1].split(")")[0])
        if "MEM_SLOW_WLAT" in line:
          m_slow_wlat = int(line.split("(")[1].split(")")[0])

  # Xp description
  print("Dycton XP preparation script")
  print("============================")
  print("author: T. Delizy (delizy.tristan@gmail.com)")
  print(date)
  print("\n\nConfiguration:")
  print("==============")
  print("targeted softwares:")
  for s in xp.target_sw:
    print("\t", s)
  print("\ntargeted architectures:")
  print("\t " + ", ".join(xp.target_hw))
  print("\ntargeted strategies for dynamic memory allocation:")
  for s in xp.target_strats:
    print("\t", s)
  print("\ntarget datasets:")
  print("\t " + ", ".join(map(str, xp.target_datasets)))
  print("\nheap memory latencies (cycles) - src/platform_tlm/address_map.h:")
  for l in amap_lines:
  	print("\t", l)

  profile_target_datasets = []
  if any(x in xp.target_strats for x in ["profile"]):
    profile_target_datasets = [x for x in [0, 1, 2, 3, 4, 5, 6, 7] if x not in xp.target_datasets]
    print("\nprofile will be constructed on following datasets:")
    print("\t " + ", ".join(map(str, profile_target_datasets)))

  print("\n\nStarting Xp preparation")
  print("=======================")


  # Xp data folder creation
  print("> creating xp folder:", end=' ', flush=True)
  xp_folder = f"dycton_xp_{date}"
  log_folder = path.join(xp_folder, "logs")
  ref_exec_folder = path.join(xp_folder, "ref_exec")
  ref_subproc = []
  name_subproc = []
  print(xp_folder, "...", end=' ')
  try:
    mkdir(xp_folder)
    mkdir(log_folder)
    mkdir(ref_exec_folder)
    mkdir(path.join(xp_folder, "results"))
  except:
    print("error in experience folders creation.")
    return 1
  print("done.")


  # generating experiment architecture description files needed for density computation
  with open(path.join(ref_exec_folder, "target_memory"), 'w+') as outfile:
    outfile.write(f"HEAP_0:0:0:{m_fast_rlat}:{m_fast_wlat}\n")
    outfile.write(f"HEAP_0:0:0:{m_slow_rlat}:{m_slow_wlat}\n")


  # prepare sim env
  print("> prepare clean simulation environment ...", end=' ', flush=True)
  # create folders and copy input files
  try:
    mkdir(path.join(xp_folder, "clean_env"))
    mkdir(path.join(xp_folder, "clean_env/iss"))
    mkdir(path.join(xp_folder, "clean_env/logs"))
    mkdir(path.join(xp_folder, "clean_env/software"))
    copyfile("src/platform_tlm/rng_sim.txt", path.join(xp_folder, "clean_env/rng_sim.txt"))
  except:
    print("error in environment folders creation.")
    return 1

  for sw in xp.target_sw:
    try:
      mkdir(path.join(xp_folder, f"clean_env/software/{sw}"))
      if sw not in ["jpeg", "json_parser", "ecdsa", "h263", "dummy_memset"]:
        copyfile(f"src/platform_tlm/software/{sw}/LICENSE", path.join(xp_folder, f"clean_env/software/{sw}/LICENSE"))
      elif sw in ["h263"]:
        copyfile(f"src/platform_tlm/software/{sw}/COPYING", path.join(xp_folder, f"clean_env/software/{sw}/COPYING"))
      if sw not in ["ecdsa", "dummy_memset"]:
        copytree(f"src/platform_tlm/software/{sw}/datasets", path.join(xp_folder, f"clean_env/software/{sw}/datasets"))
    except:
      print(f"error in {sw} file setup.")
      return 1
  print("done.")

  # setup run script and embedd configuration file
  print("> embedding run script and configuration file ...", end=' ', flush=True)

  try:
    copyfile("xp_config.py", path.join(xp_folder, "xp_config.py"))
    copyfile("src/scripts/dycton_run_xp.py", path.join(xp_folder, "dycton_run_xp.py"))
    chmod(path.join(xp_folder, "dycton_run_xp.py"), 0o744)
    chmod(path.join(xp_folder, "xp_config.py"), 0o744)
  except:
    print("error in copying config and run script to experiment folder.")
    return 1

  print("done.")

  # generating reference executions for experiments
  if xp.target_strats != ["baseline"]:
    print("> generating reference executions ...", end=' ', flush=True)
    with open(path.join(log_folder, "ref_exec.log"), 'w+') as outfile:

      # one reference execution by dataset
      # if profile strategy is involved, we also need to execute reference for profile computation
      if any(x in xp.target_strats for x in ["profile"]):
        ref_exec_datasets = [0, 1, 2, 3, 4, 5, 6, 7]
      else:
        ref_exec_datasets = xp.target_datasets

      # create sw folders in reference execution
      for sw in xp.target_sw:
        sw_ref = path.join(ref_exec_folder, sw)
        
        try:
          mkdir(sw_ref)
          for i in ref_exec_datasets:
            mkdir(path.join(sw_ref, f"_d{i}"))
        except:
          print(f"error in reference execution folders creation for {sw_ref}.")
          return 1

        # build simulators and embedded softwares
        p = subprocess.Popen(["make", "-B", f"DY_SOFT={sw}"], cwd="src/platform_tlm/iss/", stdout=outfile, stderr=subprocess.STDOUT)
        p.wait()
        if p.returncode:
          print(f"error in simulator compilation for reference executions for {sw}.")
          return 1

        ref_sw_tmp_run = path.join(sw_ref, "tmp_run")
        try:
          mkdir(ref_sw_tmp_run)
        except:
          print(f"error in temporary run folder creation to {ref_sw_tmp_run}.")
          return 1

        for i in ref_exec_datasets:
          # create temporary execution directpries
          dataset_string = f"_d{i}"
          dataset_sw_tmp_run = path.join(ref_sw_tmp_run, dataset_string)

          try:
            copytree(path.join(xp_folder, "clean_env"), dataset_sw_tmp_run)
          except:
            print(f"error in reference execution setup to {dataset_sw_tmp_run}.")
            return 1

          # move simulator and embedded soft to the reference execution temporary folder
          sim_run_x = path.join(dataset_sw_tmp_run, f"iss/{sw}_sim.x")
          try:
            copyfile("src/platform_tlm/iss/run.x", sim_run_x)
          except:
            print(f"error in moving run.x to {sim_run_x}.")
            return 1

          sim_a_out = path.join(dataset_sw_tmp_run, "software/a.out")
          try:
            copyfile("src/platform_tlm/software/a.out", sim_a_out)
          except:
            print(f"error in moving a.out to {sim_a_out}.")
            return 1

          # chmod +x simulator
          try:
            chmod(sim_run_x, 0o744)
          except:
            print(f"error in simulator execution permission elevation for {sim_run_x}.")
            return 1

          # run reference execution
          with open(path.join(sw_ref, dataset_string, f"{sw}{dataset_string}_run.result"), 'w+') as resfile:
            p = subprocess.Popen([f"./{sw}_sim.x", "-c", "-a", "-1", "-d", str(i)], cwd=path.join(dataset_sw_tmp_run, "iss"), stdout=resfile, stderr=subprocess.STDOUT)
            ref_subproc.append(p)
            name_subproc.append(sw)

        # move symbol table and disassembly to ref_exec result folder
        try:
          move(f"src/platform_tlm/software/{sw}/cross/symbol_table.txt", path.join(sw_ref, "symbol_table.txt"))
          move(f"src/platform_tlm/software/{sw}/cross/a.asm", path.join(sw_ref, "a.asm"))
        except:
          print(f"error in moving symbol table and disassembly to {sw_ref}.")
          return 1

      # wait xp_end
      return_code = 0
      for name, process in zip(name_subproc, ref_subproc):
        if return_code == 0 and process.wait() != 0:
          return_code = 1
          print(f"error in reference execution run for software {name}.")
        elif return_code == 1:
          process.kill()
      if return_code == 1:
        return 1

      # retrieve results
      for sw in xp.target_sw:
        sw_ref = path.join(ref_exec_folder, sw)
        ref_sw_tmp_run = path.join(sw_ref, "tmp_run")
        for i in ref_exec_datasets:
          # create temporary execution directories
          dataset_string = f"_d{i}"
          dataset_sw_tmp_run = path.join(ref_sw_tmp_run, dataset_string)
          
          try:
            move(path.join(dataset_sw_tmp_run, "logs/heap_objects.log"), path.join(sw_ref, f"{dataset_string}"))
          except:
            print(f"error in retreving reference execution logs from {path.join(dataset_sw_tmp_run, 'logs')}.")
            return 1 

        # cleanup
        #try:
        #  rmtree(ref_sw_tmp_run, ignore_errors=True)
        #except:
        #  print(f"error in cleaning up {ref_sw_tmp_run}.")
        #  return 1
    print("done.")



  # profile computation on half of datasets (datasets that are not target dataset)
  profile_folder = path.join(xp_folder, "profiles")
  if any(x in xp.target_strats for x in ["profile"]):
    print("> compute profiles for online strategy...", end=' ', flush=True)
    # create profile folder
    
    try:
      mkdir(profile_folder)
    except:
      print("error in profile folder creation.")
      return 1

    profile_gen_log = path.join(log_folder, 'profile_gen.log')
    with open(profile_gen_log, 'w+') as outfile:
      for sw in xp.target_sw:

        try:
          mkdir(path.join(profile_folder, sw))
        except:
          print(f"error in profile folder {sw} creation.")
          return 1

        for i in profile_target_datasets: # /!\ not target datasets !
          dataset_string = f"_d{i}"
          # compute profiles from reference executions
          dataset_sw_ref = path.join(ref_exec_folder, sw, dataset_string)
          with redirect_stdout(outfile):
            ret = analyse_object(profile_run=True,
              log_file=path.join(dataset_sw_ref, "heap_objects.log"),
              symbol_table_file=path.join(ref_exec_folder, sw, "symbol_table.txt"),
              memory_arch_file=path.join(ref_exec_folder, "target_memory"),
              output_file=path.join(profile_folder, sw, f"{sw}_alloc_site{dataset_string}.profile"))

          if ret != 0:
            print(f"error in profile computation, see log in {profile_gen_log}.")
            return 1

    print("done.")

  # Xp architecture descriptors  generation
  print("> generating architecture descriptors files ...", end=' ', flush=True)
  arch_gen_log = path.join(log_folder, "arch_gen.log")
  with open(arch_gen_log, 'w+') as outfile:
    with redirect_stdout(outfile):
      ret = generate_executable(res_folder=xp_folder, arch_desc_only=True)
    if ret != 0:
      print(f"error in architecture descriptors generation, see log in {arch_gen_log}.")
      return 1
  print("done.")

  # offline placement generation and ilp profile computation
  if any(re.match(r"(^ilp_100$)|(^ilp_\d{1,2}$)|(^ilp_upper_bound$)|(^density_100$)|(^density_\d{1,2}$)|(^profile$)", x) for x in xp.target_strats):
    print("> compute heap placements for offline strategies ...", end=' ', flush=True)
    placement_gen_log = path.join(log_folder, "placement_gen.log")
    with open(placement_gen_log, 'w+') as outfile:
      with redirect_stdout(outfile):
        ret = generate_placement(xp_folder=xp_folder)
      if ret != 0:
        print(f"error in placement generation, see log in {placement_gen_log}.")
        return 1
    print("done.")


  # header generation
  if any(x in xp.target_strats for x in ["profile"]):
    print("> generate headers from profiles...", end=' ', flush=True)
    header_gen_log = path.join(log_folder, "header_gen.log")
    with open(header_gen_log, 'w+') as outfile:
      for sw in xp.target_sw:
        sw_profile_folder = path.join(profile_folder, sw)
        # generate profile headers into profile folder
        with redirect_stdout(outfile):
          ret = generate_header_profile(profile_path=sw_profile_folder)
        if ret != 0:
          print(f"error in header generation, see log in {header_gen_log}.")
          return 1

        # update profile headers in repo
        try:
          copyfile(path.join(sw_profile_folder, f"{sw}_alloc_site_profile.h"),  f"src/platform_tlm/software/{sw}/{sw}_alloc_site_profile.h")
        except:
          print(f"error in copying new profile header to repo for {sw}.")
          return 1
  
    print("done.")



  # Xp executables  generation
  print("> generating executable files ...", end=' ', flush=True)
  exec_gen_log = path.join(log_folder, "exec_gen.log")
  with open(exec_gen_log, 'w+') as outfile:
    with redirect_stdout(outfile):
      ret = generate_executable(res_folder=xp_folder, exec_only=True)
    if ret != 0:
      print(f"error in executables generation, see log in {exec_gen_log}.")
      return 1
  print("done.")

  print("")
  print("")
  print("Summary")
  print(f"End time : {datetime.now()}")
  print("=======")
  print("preparation completed without errors")
  print("to run dycton simulation:")
  print("\t- go to", xp_folder)
  print("\t- if running on another machine give execution rights to dycton_run_xp.py")
  print("\t- run dycton_run_xp.py script")
  print("")
  print("results will be stored inside xp folder.")
  print("exiting...")
  return 0



if __name__ == "__main__":
  # command line arguments processing
  parser = argparse.ArgumentParser()
  args = parser.parse_args()
  return_val = experiment_preparation(**vars(args))
  sys.exit(return_val)
