# DYCTON (DYnamiC non-volaTile memOry maNagement for embedded systems.)

DYCTON is a simulation platform based on an custom Mips32 architecture integrated with SoCLib and used to benchmark the impact of variant form of memory setup, more precisely with a portion of fast memory based on the size of the app footprint and a slow memory with the size of the app footprint.

## Installation

___We only are using Linux for this project___

### SystemC

```bash
wget https://www.accellera.org/images/downloads/standards/systemc/systemc-2.3.1a.tar.gz
tar xzvf systemc-2.3.1a.tar.gz
cd systemc-2.3.1a
./configure --prefix=/usr/local/systemc
make
sudo make install
```

Now you should setup an environment variable in your shell called SYSTEMCROOT and make it equal to "/usr/local/systemc".

### crosstool-ng

Proceed to install crosstool-ng using this [link](https://crosstool-ng.github.io/docs/install/)
```bash
wget http://crosstool-ng.org/download/crosstool-ng/crosstool-ng-1.24.0.tar.xz
xz -d crosstool-ng-1.24.0.tar.xz
cd crosstool-ng-1.24.0/
./boostrap
./configure --prefix=/some/place
make
make install
export PATH="${PATH}:/some/place/bin"
```

then build the crosscompiler:
```bash
cd documents/platform_documentation/install
ct-ng build
```

### python-env

Get back at the root of the repository and type one of the following command.

```bash
source dycton-python-env/bin/activate # Using Bash/Zsh
source dycton-python-env/bin/activate.fish # Using Fish
source dycton-python-env/bin/activate.csh # Using Csh/Tcsh
source dycton-python-env/bin/Activate.ps1 # Using PowerShell Core
```
#pip install docplex
## Usage

```bash
emacs ./xp_config.py # Edit this file to specify experiments' parameters
./xp_preparation.py # To prepare dycton's experiments folder
cd dycton_xp_YYYY-MM-DD_hh:mm:ss # Jump to the folder specified by the ./xp_preparation.py script
./dycton_run_xp.py # Run all the simulations

../src/scripts/dycton_result_synthesis.py results_raw # Generate a CSV with all of the simulations' infos
../src/scripts/dycton_plot.py results/result_synthesis.csv # Plot dycton's performance graphs into graphs folder
../src/scripts/log_process.py [results_raw/logs/${application}_arch${architecture}_d${dataset}_${strategy} ...]
# You can explore all of the others script in ../serc/scripts with --help to show script's usage.
```

## Information

For git repository management best practices refer to this [link](https://sethrobertson.github.io/GitBestPractices)

##
 CPLEX et docplex.py
##
 get cplex_studio129.linux-x86-64.bin with DownloadDirector (check help elsewhere)
 execute cplex_studio129.linux-x86-64.bin and install in directory DIR
 go in DIR/python
 sudo python setup.py install
 --> example use:
 ../src/scripts/ilp_from_trace.py -i results_raw/logs/dijkstra_arch10_d0_ilp_run/heap_objects.log -a results_raw/logs/dijkstra_arch10_d0_ilp_run/memory_architecture 

##
login on luuna
##
Activer le vpn insa (sudo openconnect sslvpn.insa-lyon.fr)
le loguer sur luuna avec mdp insa (ssh luuna.citi.insa-lyon.fr)


## Contact

- tdelizy@insa-lyon.fr
- alexandre.monierc@gmail.com
- guillaume.salagnac@insa-lyon.fr
- matthieu.moy@inria.fr
